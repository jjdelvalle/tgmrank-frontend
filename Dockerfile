FROM node:12-alpine as builder

ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV}

RUN apk add --no-cache make gcc g++ curl

WORKDIR tgmrank-frontend

COPY ./package.json ./package.json
COPY ./yarn.lock ./yarn.lock
RUN NODE_ENV=development CYPRESS_INSTALL_BINARY=0 yarn install

ADD . ./

RUN yarn test:unit
RUN yarn compile
RUN yarn build:all

# prune non-production packages
RUN yarn install --production

FROM node:12-alpine
WORKDIR tgmrank-frontend
COPY --from=builder /tgmrank-frontend ./
