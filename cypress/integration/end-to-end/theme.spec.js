function clickThemeToggler() {
  cy.get('[data-action="toggle-theme"]').click();
}

function assertDarkTheme(value) {
  const chainer = value ? 'have.class' : 'not.have.class';
  cy.get('html > body').should(chainer, 'dark-theme');
}

describe('Theme', () => {
  it('Should toggle when dark mode toggle is clicked', () => {
    cy.visit('/');

    assertDarkTheme(false);
    clickThemeToggler();
    assertDarkTheme(true);
    clickThemeToggler();
    assertDarkTheme(false);
  });
});
