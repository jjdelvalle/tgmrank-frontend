describe('Recent Activity', () => {
  const defaultPageSize = 10;

  describe('Home Page', () => {
    it('Should link to score page from recent activity', () => {
      cy.visit('/');
      cy.get('.activity :nth-child(1) > .activity-item').click('left');

      cy.get('.player-page').should('be.visible');
      cy.get('.score-details__main').should('be.visible');
    });

    it('Should link to player page from recent activity', () => {
      cy.visit('/');
      cy.get(
        '.activity :nth-child(1) > .activity-item .profile-group',
      ).click();

      cy.get('.player-scores').should('be.visible');
    });

    it('should load initial results', () => {
      cy.visit('/');
      cy.get('.activity')
        .find('.activity-item__top')
        .should('have.length', defaultPageSize);
    });

    it('should load more pages when infinite scroll button is clicked', () => {
      cy.visit('/');

      cy.scrollTo('bottom');
      cy.get('[data-info="infinite-scroll-start"]').click();
      cy.get('.activity')
        .find('.activity-item__top')
        .should('have.length', defaultPageSize * 2);

      cy.scrollTo('bottom');
      cy.get('.activity')
        .find('.activity-item__top')
        .should('have.length.gte', defaultPageSize * 2);
    });
  });

  describe('Player Page', () => {
    it('should load more pages and show message when no more results are found', () => {
      cy.visit('/tgm1/big');

      cy.get(
        '.leaderboard :nth-child(1) > .leaderboard__item--player > span > a',
      ).click();

      cy.get('.activity')
        .find('.activity-item__top')
        .should('have.length.gte', 0);

      cy.get('[data-info="infinite-scroll-start"]').click();

      cy.scrollTo('bottom');
      cy.scrollTo('bottom');
      cy.scrollTo('bottom');

      cy.get('[data-info="infinite-scroll-end"]');
    });
  });
});
