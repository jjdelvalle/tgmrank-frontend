function getNavLink(tag) {
  return cy.get(`.nav [data-bold-toggle="${tag}"]`);
}

function navIsActive(tag) {
  getNavLink(tag).should('have.class', 'nav-link--active');
}

describe('Navigation', () => {
  describe('Header', () => {
    function validateHeaderNav(navTag, expectedTarget) {
      cy.visit('/');

      cy.get(`[data-info="${navTag}"]`).click();
      cy.validatePath(expectedTarget);

      cy.get(`[data-info="${navTag}"]`).should(
        'have.class',
        'nav-link--active',
      );
    }

    it('should navigate to home page when clicking on logo', () => {
      cy.visit('/about');

      cy.get('.header .logo').should('be.visible').click();
      cy.validatePath('/');
    });

    it('should navigate to overall game page', () => {
      validateHeaderNav('overall', '/overall');
    });

    it('should navigate to tgm game page', () => {
      validateHeaderNav('tgm1', '/tgm1');
    });

    it('should navigate to tap game page', () => {
      validateHeaderNav('tgm2', '/tap');
    });

    it('should navigate to ti game page', () => {
      validateHeaderNav('tgm3', '/ti');
    });

    it('should navigate to event archive', () => {
      validateHeaderNav('archive', '/event/archive');
    });
  });

  describe('policies', () => {
    it('should be able to navigate to proof policy', () => {
      cy.visit('/about/proof');
      cy.get('.proof-policy').should('exist');
      cy.validatePath('/about/proof');

      cy.visit('/about');
      cy.get('.about [href="/about/proof"]').click();
      cy.validatePath('/about/proof');
    });

    it('should be able to navigate to privacy policy', () => {
      cy.visit('/about/privacy');
      cy.get('.privacy-policy').should('exist');
      cy.validatePath('/about/privacy');

      cy.visit('/about');
      cy.get('.about [href="/about/privacy"]').click();
      cy.validatePath('/about/privacy');
    });
  });

  describe('Leaderboard', () => {
    it('should redirect to 404 page when specified game does not exist', () => {
      cy.visit('/my-cool-game');
      cy.get('.leaderboard').should('not.exist');
      cy.validatePath('/not-found');
    });

    describe('Overall', () => {
      it('should navigate between aggregate leaderboards', () => {
        // TODO: Check each leaderboard loads?
        cy.visit('/overall');
        navIsActive('main');

        getNavLink('extended').click();
        navIsActive('extended');

        getNavLink('secret').click();
        navIsActive('secret');

        getNavLink('big').click();
        navIsActive('big');

        getNavLink('main').click();
        navIsActive('main');
      });

      it('should redirect to 404 page when leaderboard does not exist', () => {
        cy.visit('/overall/hello-world');
        cy.get('.leaderboard').should('not.exist');
        cy.validatePath('/not-found');
      });

      it('should redirect to game pages from aggregate leaderboard descriptions', () => {
        cy.visit('/overall');
        cy.get('[data-bold-toggle="big"] .badge').click();

        // TODO: Use a better css selector
        cy.get(':nth-child(3) > :nth-child(2) > [href="/tap"]').click();

        cy.validatePath('/tap');
        navIsActive('main');
      });
    });

    describe('TGM1', () => {
      it('should be able to navigate directly to mode leaderboard', () => {
        cy.visit('/tgm1/master');
        cy.get('.leaderboard').should('be.visible');
      });

      it('should redirect to 404 page when leaderboard does not exist', () => {
        cy.visit('/tgm1/hello-world');
        cy.get('.leaderboard').should('not.exist');
        cy.validatePath('/not-found');
      });

      it('should navigate between aggregate leaderboards and mode leaderboards', () => {
        cy.visit('/tgm1/extended');
        navIsActive('extended');

        getNavLink('main').click();
        navIsActive('main');

        getNavLink('20G').click();
        navIsActive('20G');

        getNavLink('Big').click();
        navIsActive('Big');
      });
    });
  });

  describe('Score', () => {
    function assertOnScorePage() {
      cy.get('.player-page').should('be.visible');
      cy.get('.score-details__grid').should('be.visible');
      cy.get('.score-details__info').should('be.visible');
    }

    it('should be able to navigate to score page from recent activity', () => {
      cy.visit('/');
      cy.get('.activity :nth-child(1) > .activity-item').click('left');
      assertOnScorePage();
    });

    it('should be able to navigate to score page from mode leaderboard', () => {
      cy.visit('/tgm1/master');
      cy.get('.leaderboard > .leaderboard__item:first-child').click();
      assertOnScorePage();
    });

    it('should be able to navigate directly to a score page', () => {
      cy.visit('/score/1');
      assertOnScorePage();
    });
  });

  describe('Player', () => {
    function assertOnPlayerPage() {
      cy.get('.player-page').should('be.visible');
    }

    function getPlayerTab(tab) {
      return cy.get(`[data-bold-toggle="${tab}"]`);
    }

    function assertOnPlayerTab(tab, visibleElement) {
      cy.get(visibleElement).should('be.visible');

      getPlayerTab(tab).should('have.class', 'nav-link--active');
    }

    it('should be able to navigate directly to player pages', () => {
      cy.visit('/tgm1/master');
      cy.get(
        '.leaderboard :nth-child(1) > .leaderboard__item--player > span > a',
      ).then($element => {
        const username = $element.text();
        cy.visit(`/player/${username}`);

        assertOnPlayerPage();
      });
    });

    it('should navigate to player achievements tab', () => {
      cy.visit('/tgm1/master');
      cy.get(
        '.leaderboard :nth-child(1) > .leaderboard__item--player > span > a',
      ).then($element => {
        const username = $element.text();
        cy.visit(`/player/${username}/achievements`);

        assertOnPlayerTab('Achievements', '.achievements-list');

        cy.get('.avatar').click();
        assertOnPlayerPage();
        cy.validatePath(`/player/${username}`);

        getPlayerTab('Achievements').click();
        assertOnPlayerTab('Achievements', '.achievements-list');
      });
    });

    it('should navigate to player rivals tab', () => {
      cy.visit('/tgm1/master');
      cy.get(
        '.leaderboard :nth-child(1) > .leaderboard__item--player > span > a',
      ).then($element => {
        const username = $element.text();
        cy.visit(`/player/${username}/rivals`);

        assertOnPlayerTab('Rivals', '.rival-table');

        cy.get('.avatar').click();
        assertOnPlayerPage();
        cy.validatePath(`/player/${username}`);

        getPlayerTab('Rivals').click();
        assertOnPlayerTab('Rivals', '.rival-table');
      });
    });

    it('should be able to navigate to player pages from profile groups', () => {
      cy.visit('/');
      cy.get(
        '.activity :nth-child(1) > .activity-item .profile-group',
      ).click();

      assertOnPlayerPage();
    });

    it('should be able to navigate to player pages from mode leaderboards', () => {
      cy.visit('/tgm1/master');
      cy.get(
        '.leaderboard :nth-child(1) > .leaderboard__item--player > span > a',
      ).click();

      assertOnPlayerPage();
    });
  });
});
