import React from 'react';
import useSWR from 'swr';
import { RankedModeKeys } from '../hooks/use-games';
import TgmRank from '../tgmrank-api';
import LeaderboardRank from './leaderboard/rank';

export function UserRanking({ playerId }) {
  const { data: overallRankingData } = useSWR(
    playerId != null
      ? TgmRank.getAggregateRankingForPlayerUrl(
          0,
          RankedModeKeys.main,
          playerId,
        ).toString()
      : null,
  );

  const rankingData = overallRankingData?.[0];
  const rank = rankingData?.rank;
  const points = rankingData?.rankingPoints ?? 0;

  return (
    <div>
      {rank != null && (
        <>
          <LeaderboardRank rank={rank} /> |
        </>
      )}{' '}
      {points} Points
    </div>
  );
}
