import React from 'react';
import { Trans } from '@lingui/macro';
import TimeDelta from './time-delta';
import { GradeDisplay } from './numbers';

export default function({ item, mode }) {
  if (item.delta?.mode?.rank == null && item.previousScore != null) {
    return null;
  }

  return (
    <>
      {[12, 17, 22].includes(mode.modeId) ? (
        item.previousScore?.score &&
        item.score?.score > item.previousScore?.score && (
          <div>
            <Trans>
              Score improved by{' '}
              <b>{item.score?.score - item.previousScore?.score} points</b>
            </Trans>
          </div>
        )
      ) : (
        <>
          {item.previousScore?.gradeOrder &&
            item.previousScore.gradeOrder < item.score.gradeOrder && (
              <div>
                <Trans>
                  Grade increased from{' '}
                  <GradeDisplay score={item.previousScore}>
                    <b>{item.previousScore.gradeDisplay}</b>
                  </GradeDisplay>{' '}
                  to{' '}
                  <GradeDisplay score={item.score}>
                    <b>{item.score.gradeDisplay}</b>
                  </GradeDisplay>
                </Trans>
              </div>
            )}
          {item.previousScore?.level &&
            item.previousScore.level < item.score.level && (
              <div>
                <Trans>
                  Level improved by{' '}
                  <b>{item.score.level - item.previousScore.level} levels</b>
                </Trans>
              </div>
            )}
        </>
      )}
      {item.previousScore?.playtime && (
        <TimeDelta
          previous={item.previousScore.playtime}
          current={item.score.playtime}
        />
      )}
    </>
  );
}
