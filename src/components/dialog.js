import React from 'react';
import { Dialog as ReachDialog } from '@reach/dialog';

export default function Dialog({ close, ...props }) {
  return (
    <ReachDialog {...props} onDismiss={() => close()}>
      <button className="dialog-close-button" onClick={() => close()}>
        {/* <VisuallyHidden>Close</VisuallyHidden> */}
        <span aria-hidden>×</span>
      </button>
      {props.children}
    </ReachDialog>
  );
}
