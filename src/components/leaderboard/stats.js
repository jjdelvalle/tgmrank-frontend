import { Trans } from '@lingui/macro';
import React, { useState } from 'react';
import {
  GradeDisplay,
  LevelDisplay,
  RankingPointsDisplay,
  ScoreDisplay,
  TimeDisplay,
} from '../numbers';
import {
  GradeTitle,
  LevelTitle,
  PointsTitle,
  ScoreTitle,
  TimeTitle,
} from '../score';
import { isRequired, mustBeEmpty } from '../../mode-fields';
import Cell, { Number } from '../cell';
import objstr from 'obj-str';
import Dialog from '../dialog';
import useSWR from 'swr';
import TgmRank from '../../tgmrank-api';
import { useGames } from '../../hooks/use-games';
import { GameModeLink } from '../mode-name';

function insertIf(condition, ...elements) {
  return condition ? elements : [];
}

function MaybeWrap({ condition, wrapper, children }) {
  return condition ? wrapper(children) : children;
}

export function StatsRow({ stats, item, ...props }) {
  return (
    <div
      className={objstr({
        'stats-row': true,
        'stats-row--long': stats.length > 1,
      })}
      {...props}
    >
      {stats.map(({ stat, title }, index) => (
        <Number key={index} stat={stat(item)} title={title} />
      ))}
    </div>
  );
}

function RankingPoints({ score, game, rankedModeKey }) {
  const [showDialog, setState] = useState(false);
  return (
    <>
      <MaybeWrap
        condition={game != null}
        wrapper={children => (
          <a
            onClick={event => {
              event.stopPropagation();
              event.preventDefault();
              setState(true);
            }}
          >
            {children}
          </a>
        )}
      >
        <RankingPointsDisplay
          points={score.rankingPoints}
          rankedScoreCount={score.scoreCount}
        />
      </MaybeWrap>
      <Dialog
        aria-label="Close Contributing Scores dialog"
        isOpen={showDialog}
        close={() => setState(false)}
      >
        <AggregateScoreSummary
          playerId={score?.player?.playerId}
          game={game}
          rankedModeKey={rankedModeKey}
        />
      </Dialog>
    </>
  );
}

function AggregateScoreSummary({ playerId, game, rankedModeKey }) {
  const { data: pbData } = useSWR(
    playerId != null ? TgmRank.getMultiplePlayerPbsUrl([playerId]) : null,
  );

  const { games } = useGames();

  const modeIds = game?.rankedModes?.[rankedModeKey];

  const relevantPbs = pbData
    ?.filter(pb => modeIds?.includes(pb.modeId))
    .map(pb => {
      const { game, mode } = games.lookup?.(pb.gameId, pb.modeId);
      return { ...pb, game, mode };
    })
    .sort((a, b) => a.gameId - b.gameId || a.mode.sortOrder - b.mode.sortOrder);

  const missingModes = modeIds
    .filter(modeId => !pbData?.some(pb => modeId === pb.modeId))
    .map(modeId => games.lookupMode(modeId))
    .sort((a, b) => a.game.gameId - b.game.gameId || a.sortOrder - b.sortOrder);

  return (
    <div>
      <h1>
        <Trans>Contributing Scores</Trans>
      </h1>
      <ul className="list-bare">
        {relevantPbs?.map(pb => {
          const stats = getStats({
            mode: pb.mode,
          });
          const score = { ...pb, player: null };
          return (
            <li key={pb.scoreId} className="contributing-scores__item">
              <div className="contributing-scores__link">
                <GameModeLink
                  game={pb.game}
                  mode={pb.mode}
                  rankedModeKey={rankedModeKey}
                />
              </div>
              <Cell stats={stats} score={score} />
            </li>
          );
        })}
      </ul>
      {pbData != null && missingModes?.length > 0 && (
        <>
          <h1>
            <Trans>Missing Scores</Trans>
          </h1>
          <ul>
            {missingModes?.map(mode => {
              return (
                <li key={mode.modeId}>
                  <div className="contributing-scores__link">
                    <GameModeLink
                      game={mode.game}
                      mode={mode}
                      rankedModeKey={rankedModeKey}
                    />
                  </div>
                </li>
              );
            })}
          </ul>
        </>
      )}
    </div>
  );
}

export default function getStats({
  game,
  mode,
  rankedModeKey,
  showOptional = false,
  exclude: excludedFields = [],
}) {
  const showCondition = requiredStatus => {
    if (requiredStatus == null) {
      return false;
    }
    if (showOptional) {
      return !mustBeEmpty(requiredStatus);
    }
    return isRequired(requiredStatus);
  };

  return [
    ...insertIf(
      !excludedFields.includes('rankingPoints') &&
        (mode == null || mode?.isRanked),
      {
        title: <PointsTitle />,
        stat: score => {
          return (
            <RankingPoints
              score={score}
              game={game}
              rankedModeKey={rankedModeKey}
            />
          );
        },
      },
    ),
    ...insertIf(showCondition(mode?.gradeRequired), {
      title: <GradeTitle modeId={mode?.modeId} />,
      stat: score => (
        <GradeDisplay grade={score.grade}>
          {score.grade?.gradeDisplay}
        </GradeDisplay>
      ),
    }),
    ...insertIf(showCondition(mode?.scoreRequired), {
      title: <ScoreTitle modeId={mode?.modeId} />,
      stat: score => <ScoreDisplay score={score.score} />,
    }),
    ...insertIf(showCondition(mode?.levelRequired), {
      title: <LevelTitle modeId={mode?.modeId} />,
      stat: score => <LevelDisplay level={score.level} />,
    }),
    ...insertIf(showCondition(mode?.timeRequired), {
      title: <TimeTitle />,
      stat: score => <TimeDisplay time={score.playtime} />,
    }),
  ];
}
