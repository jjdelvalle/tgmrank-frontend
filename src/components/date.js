import React from 'react';
import { i18n } from '@lingui/core';

export default function LongDate({ children }) {
  const options = { year: 'numeric', month: 'long', day: 'numeric' };
  return dateTime(new Date(children), options);
}

export function LongDateTime({ children }) {
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    second: 'numeric',
  };
  return dateTime(new Date(children), options);
}

function dateTime(date, options) {
  const dateString = date.toISOString();
  return (
    <time title={dateString} dateTime={dateString}>
      {i18n.date(date, options)}
    </time>
  );
}
