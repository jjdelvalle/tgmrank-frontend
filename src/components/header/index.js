import React, { useRef } from 'react';
import { Link, Location } from '@reach/router';
import { PlusCircle as IconPlus } from 'react-feather';
import { Helmet } from 'react-helmet-async';
import {
  Menu,
  MenuButton,
  MenuItem,
  MenuItems,
  MenuLink,
  MenuPopover,
} from '@reach/menu-button';
import { positionRight } from '@reach/popover';
import { Trans } from '@lingui/macro';
import HeaderLink from '../header-link';
import ProfileGroup from '../profile-group';
import LanguageSwitcherIcon from '../../icons/language-switcher';
import Chevron from '../../icons/chevron';
import DarkThemeIcon from '../../icons/dark-theme';
import { useLanguage } from '../../contexts/language';
import { useTheme } from '../../contexts/theme';
import { canVerifyScores, isAdmin, useUser } from '../../contexts/user';
import TgmRank from '../../tgmrank-api';
import { UserRanking } from '../user-ranking';

import Logo from '../../../images/logo';

import './nav.css';
import './styles.css';

export default function Header() {
  const { darkTheme, setDarkTheme } = useTheme();
  const { changeLanguage } = useLanguage();
  const { user, isLoggedIn } = useUser();

  const menuButtonRef = useRef();

  return (
    <header className="header">
      <div className="header-logo">
        <Link to="/">
          <Logo />
        </Link>
      </div>
      <nav className="header__game-nav nav">
        <HeaderLink to="/overall" data-info="overall">
          <Trans>Overall</Trans>
        </HeaderLink>
        <HeaderLink to="/tgm1" data-info="tgm1">
          TGM
        </HeaderLink>
        <HeaderLink to="/tap" data-info="tgm2">
          TAP
        </HeaderLink>
        <HeaderLink to="/ti" data-info="tgm3">
          TI
        </HeaderLink>
        <HeaderLink to="/event/archive" data-info="archive">
          <Trans>Archive</Trans>
        </HeaderLink>
      </nav>
      <div className="nav header__main-nav">
        <button
          data-action="toggle-theme"
          className="nav-button"
          onClick={() => setDarkTheme(!darkTheme)}
        >
          <DarkThemeIcon className="nav__icon nav__icon--moon" />
        </button>
        <Menu className="header__language-switcher">
          <MenuButton className="nav-button">
            <LanguageSwitcherIcon className="nav__icon nav__icon--translate" />{' '}
            <Chevron width={10} />
          </MenuButton>
          <MenuPopover className="dropdown">
            <MenuItems>
              <MenuItem onSelect={() => changeLanguage('en')}>English</MenuItem>
              <MenuItem onSelect={() => changeLanguage('fr')}>
                Français
              </MenuItem>
              <MenuItem onSelect={() => changeLanguage('ja')}>日本語</MenuItem>
              <MenuItem onSelect={() => changeLanguage('de')}>Deutsch</MenuItem>
              <MenuItem onSelect={() => changeLanguage('pt')}>
                Português
              </MenuItem>
              <MenuItem onSelect={() => changeLanguage('es')}>Español</MenuItem>
            </MenuItems>
          </MenuPopover>
        </Menu>
        {isLoggedIn ? (
          <Location>
            {({ location }) => (
              <>
                {location.pathname !== '/score/submit' && (
                  <Link
                    data-action="submit-score"
                    className="nav-button button button--submit-score"
                    to="/score/submit"
                  >
                    <IconPlus />{' '}
                    <span>
                      <Trans>Submit Score</Trans>
                    </span>
                  </Link>
                )}
                <Menu>
                  {({ isExpanded }) => (
                    <>
                      <MenuButton className="nav-button user-menu">
                        <div ref={menuButtonRef}>
                          <ProfileGroup
                            image={TgmRank.getAvatarLink(user.avatar, true)}
                            location={user.location}
                            data-info="user-menu"
                          />
                        </div>
                      </MenuButton>
                      <MenuPopover
                        className="dropdown dropdown--user"
                        position={positionRight}
                      >
                        {isExpanded && (
                          <Helmet>
                            <html className="nav-popover-active" />
                          </Helmet>
                        )}
                        <MenuItems>
                          <MenuLink as={Link} to={`/player/${user.username}`}>
                            <ProfileGroup
                              name={user.username}
                              subtitle={<UserRanking playerId={user?.userId} />}
                              image={TgmRank.getAvatarLink(user.avatar, true)}
                              location={user.location}
                            />
                          </MenuLink>
                          <MenuLink as={Link} to={`/player/${user.username}`}>
                            <Trans>Profile</Trans>
                          </MenuLink>
                          <MenuLink as={Link} to="/settings">
                            <Trans>Settings</Trans>
                          </MenuLink>
                          {isAdmin(user) && (
                            <MenuLink as={Link} to="/admin">
                              Admin Panel
                            </MenuLink>
                          )}
                          {canVerifyScores(user) && (
                            <MenuLink as={Link} to="/admin/verification-queue">
                              Verification Queue
                            </MenuLink>
                          )}
                          <MenuLink as={Link} to="/about" className="separator">
                            <Trans>About/Contact</Trans>
                          </MenuLink>
                          <MenuLink as={Link} to="/logout">
                            <Trans>Logout</Trans>
                          </MenuLink>
                        </MenuItems>
                      </MenuPopover>
                    </>
                  )}
                </Menu>
              </>
            )}
          </Location>
        ) : (
          <>
            <Link
              to="/sign-up"
              className="nav-button ghost-button"
              data-info="register"
            >
              <Trans>Sign up</Trans>
            </Link>
            <Link to="/login" className="nav-button" data-info="login">
              <Trans>Login</Trans>
            </Link>
          </>
        )}
      </div>
    </header>
  );
}
