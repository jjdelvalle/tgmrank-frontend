import React from 'react';

import './styles.css';

export function Progress({ percent }) {
  percent = Math.min(Math.max(percent, 0), 100).toFixed();

  return (
    <div className="progress">
      <div className="progress-inner">
        <div
          className="progress-inner__filled"
          style={{ width: `${percent}%` }}
        />
      </div>
    </div>
  );
}
