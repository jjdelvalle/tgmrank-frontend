import React, { cloneElement } from 'react';
import objstr from 'obj-str';

import './styles.css';

export function Tabs({ context, tab }) {
  return (
    <div className="tabs packed spaced">
      {context.labels.map((tabName, index) =>
        cloneElement(tab(tabName), {
          key: index,
          className: objstr({
            tab: true,
            'tab--active': index === context.selectedTab,
          }),
        }),
      )}
    </div>
  );
}

export function Panels({ context, children }) {
  return children[context.selectedTab];
}

export function TabsContainer(props) {
  const { render, ...context } = props;
  const activeTabLabel = context.labels[context.selectedTab];
  return render(context, activeTabLabel);
}
