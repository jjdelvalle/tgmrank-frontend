import { useLingui } from '@lingui/react';
import React from 'react';
import ReactMarkdown from 'react-markdown';
import directive from 'remark-directive';
import { visit } from 'unist-util-visit';
import useSWR from 'swr';
import { Trans } from '@lingui/macro';

import { Link } from '@reach/router';
import { positionRight } from '@reach/popover';

import {
  Menu,
  MenuButton,
  MenuItems,
  MenuLink,
  MenuList,
  MenuPopover,
} from '@reach/menu-button';

import { Eye as EyeIcon, Twitch as TwitchIcon } from 'react-feather';
import { leaderboardPath, useGames, ModeTags } from '../../hooks/use-games';

import Dialog from '../dialog';
import ModeName from '../mode-name';

import overall from '../../../images/decoration/huge.png';
import tgm1 from '../../../images/game-icons/tgm.png';
import tap from '../../../images/game-icons/tap.png';
import ti from '../../../images/game-icons/ti.png';
import TgmRank from '../../tgmrank-api';

import './styles.css';
import { LeaderboardFilter } from './filter';
import { AggregateLeaderboardLabel } from '../leaderboard/label';

const icons = {
  overall,
  tgm1,
  tap,
  ti,
};

function customDirectivePlugin() {
  return tree => {
    visit(tree, node => {
      if (
        node.type === 'textDirective' ||
        node.type === 'leafDirective' ||
        node.type === 'containerDirective'
      ) {
        const data = node.data || (node.data = {});
        const attributes = node.attributes || {};

        if (node.name === 'highlight') {
          data.hName = 'span';
          data.hProperties = {
            class: 'highlight',
          };
          return;
        }
      }
    });
  };
}

export function ModeDescription({ modeId, showDefault = true }) {
  const { data, error } = useSWR(
    modeId != null ? TgmRank.getModeDetailsUrl(modeId) : null,
  );

  const isLoading = !data && !error;
  if (isLoading) {
    return null;
  }

  return (
    <div className="mode-description">
      {data?.description ? (
        <ReactMarkdown remarkPlugins={[directive, customDirectivePlugin]}>
          {data.description}
        </ReactMarkdown>
      ) : (
        showDefault && (
          <Trans>
            This mode does not have formal rules yet. Be cautious and use your
            best judgment :)
          </Trans>
        )
      )}
    </div>
  );
}

function StreamCount({ game }) {
  const { games } = useGames();
  const { data: streams } = useSWR(TgmRank.twitchStreamsInfoUrl());

  if (streams == null) {
    return null;
  }

  const gameStreams = streams?.filter(
    s => game.gameId === games.overall.gameId || s.gameId === game.gameId,
  );

  const streamCount = gameStreams?.length;

  return (
    <span className="ml-1">
      <TwitchIcon size={18} />
      <span>{streamCount}</span>
    </span>
  );
}

function ViewerCount({ game }) {
  const { games } = useGames();
  const { data: streams } = useSWR(TgmRank.twitchStreamsInfoUrl());

  if (streams == null) {
    return null;
  }

  const gameStreams = streams?.filter(
    s => game.gameId === games.overall.gameId || s.gameId === game.gameId,
  );

  const totalViewers = gameStreams?.reduce(
    (sum, stream) => sum + stream.viewerCount,
    0,
  );

  return (
    <span className="ml-1">
      <EyeIcon size={18} />
      <span>{totalViewers}</span>
    </span>
  );
}

export function LiveStreams({ game }) {
  return (
    <span className="ml-1">
      <StreamCount game={game} />
      <ViewerCount game={game} />
    </span>
  );
}

export function ModeDescriptionLink({ modeId, children }) {
  const [isDialogVisible, showDialog] = React.useState(false);

  if (modeId == null) {
    return null;
  }

  return (
    <>
      <a
        onClick={event => {
          event.stopPropagation();
          event.preventDefault();
          showDialog(true);
        }}
      >
        {children ?? <Trans>View Rules</Trans>}
      </a>
      <Dialog
        aria-label="Leaderboard Rules"
        isOpen={isDialogVisible}
        close={() => showDialog(false)}
      >
        <ModeDescription modeId={modeId} />
      </Dialog>
    </>
  );
}

function MenuLinkTarget({ ...props }) {
  const isExternal = props.href.startsWith('http');
  return (
    <MenuLink
      {...props}
      target={isExternal ? '_blank' : '_self'}
      rel={isExternal ? 'noopener noreferrer' : ''}
    />
  );
}

function ExtraLinks({ game, currentMode }) {
  const extraModeLinks =
    currentMode?.links?.map((link, index) => (
      <MenuLinkTarget key={`extra${index}`} href={link.link}>
        {link.title}
      </MenuLinkTarget>
    )) ?? [];

  const menuItems = [
    game?.wikiLink && (
      <MenuLinkTarget
        key={'wiki'}
        href={game.wikiLink}
        target="_blank"
        rel="noopener noreferrer"
      >
        <Trans>Game Info</Trans>
      </MenuLinkTarget>
    ),
    game?.twitchLink && (
      <MenuLinkTarget key={'twitch'} href={game.twitchLink}>
        <Trans>Twitch</Trans>
      </MenuLinkTarget>
    ),
    currentMode?.link && (
      <MenuLinkTarget key={'legacy'} href={currentMode.link}>
        <Trans>Legacy Leaderboard</Trans>
      </MenuLinkTarget>
    ),
    ...extraModeLinks,
  ].filter(i => i != null);

  return (
    menuItems.length > 0 && (
      <Menu>
        <MenuButton>&#10247;</MenuButton>
        <MenuPopover position={positionRight}>
          <MenuItems>{menuItems}</MenuItems>
        </MenuPopover>
      </Menu>
    )
  );
}

export function LeaderboardNavigation({ game }) {
  if (game == null) {
    return null;
  }

  const eventModes = game.modes?.filter(
    m => m?.tags?.includes(ModeTags.Event) === true,
  );

  return (
    <nav className="nav nav--modes">
      {game.gameId !== 0 && (
        <div className="no-select">
          <Menu>
            <MenuButton className="nav-menu-button">
              <a className="nav-link">
                <Trans>All</Trans>
              </a>
            </MenuButton>
            <MenuList className="nav--modes__menu">
              {game?.modes
                ?.filter(m => m.isActive() || m.isRecent())
                .map(mode => (
                  <MenuLink
                    key={mode.modeId}
                    as={Link}
                    to={leaderboardPath({ game, mode })}
                  >
                    <ModeName {...mode} />
                  </MenuLink>
                ))}
            </MenuList>
          </Menu>
        </div>
      )}
      {Object.entries(game.rankedModes).map(([rankedModeKey, modeIds]) => (
        <React.Fragment key={rankedModeKey}>
          {game.gameId === 0 && (
            <Link
              className="flex nav-menu-button no-select"
              key={rankedModeKey}
              to={leaderboardPath({ game, rankedModeKey })}
            >
              <div className="nav-link">
                <AggregateLeaderboardLabel
                  game={game}
                  rankedModeKey={rankedModeKey}
                  showDescriptionButton={false}
                />
              </div>
            </Link>
          )}
          {game.gameId !== 0 && (
            <>
              <div className="no-select">
                <Menu>
                  <MenuButton className="nav-menu-button">
                    <a className="nav-link">
                      <AggregateLeaderboardLabel
                        game={game}
                        rankedModeKey={rankedModeKey}
                        showDescriptionButton={false}
                      />
                    </a>
                  </MenuButton>
                  <MenuList className="nav--modes__menu">
                    <MenuLink
                      as={Link}
                      to={leaderboardPath({ game, rankedModeKey })}
                      className="separator"
                    >
                      <AggregateLeaderboardLabel
                        game={game}
                        rankedModeKey={rankedModeKey}
                        showDescriptionButton={false}
                      />
                    </MenuLink>
                    {game?.modes
                      ?.filter(
                        m =>
                          modeIds.includes(m.modeId) &&
                          (m.isActive() || m.isRecent()),
                      )
                      .map(mode => (
                        <MenuLink
                          key={mode.modeId}
                          as={Link}
                          to={leaderboardPath({ game, mode, rankedModeKey })}
                        >
                          <ModeName {...mode} />
                        </MenuLink>
                      ))}
                  </MenuList>
                </Menu>
              </div>
            </>
          )}
        </React.Fragment>
      ))}
      {eventModes?.length > 0 && (
        <div className="no-select">
          <Menu>
            <MenuButton className="nav-menu-button">
              <a className="nav-link">
                <Trans>Events</Trans>
              </a>
            </MenuButton>
            <MenuList className="nav--modes__menu">
              {eventModes.map(mode => (
                <MenuLink
                  key={mode.modeId}
                  as={Link}
                  to={leaderboardPath({ game, mode })}
                >
                  <ModeName {...mode} />
                </MenuLink>
              ))}
            </MenuList>
          </Menu>
        </div>
      )}
      {game?.gameId === 2 && (
        <Link className="flex nav-menu-button no-select" to={'/fightcade'}>
          <div className="nav-link">Fightcade (Versus)</div>
        </Link>
      )}
    </nav>
  );
}

export default function BoardHeader({ currentGame, currentMode }) {
  const { i18n } = useLingui();
  return (
    <>
      <div className="board-header">
        <Link to={leaderboardPath({ game: currentGame, mode: currentMode })}>
          <img
            src={icons[currentGame?.shortName.toLowerCase()]}
            className="board-header__icon"
          />
        </Link>
        <div className="board-header__title">
          {currentGame?.gameName ? (
            <h1>{i18n._(currentGame?.gameName)}</h1>
          ) : (
            <h1 />
          )}
          {currentGame?.subtitle ? (
            <h3 className="jp">{currentGame?.subtitle}</h3>
          ) : (
            <h3 />
          )}
        </div>
        <div className="board-header__links">
          {currentGame?.twitchLink ? (
            <a
              href={currentGame?.twitchLink}
              target="_blank"
              rel="noopener noreferrer"
            >
              <LiveStreams game={currentGame} />
            </a>
          ) : (
            <LiveStreams game={currentGame} />
          )}
          <LeaderboardFilter />
          <div className="board-header__button">
            {currentMode && (
              <ModeDescriptionLink modeId={currentMode?.modeId} />
            )}
          </div>
          <div className="board-header__overflow-menu">
            <ExtraLinks game={currentGame} currentMode={currentMode} />
          </div>
        </div>
      </div>
    </>
  );
}
