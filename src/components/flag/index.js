import React from 'react';
import Tooltip from '@reach/tooltip';
import { useUserLocations } from '../../contexts/locations';

import './styles.css';

export default function Flag({ code }) {
  const { locations } = useUserLocations();

  code = code?.toLowerCase();
  if (code == null || locations == null) {
    return null;
  }

  const filename = `/static/flags/${code?.toLowerCase()}.svg`;
  return (
    <Tooltip label={locations.find(l => l.alpha2 === code)?.name}>
      <img className="flag" src={filename} />
    </Tooltip>
  );
}
