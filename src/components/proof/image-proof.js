import React from 'react';

export function ImageProof({ url }) {
  return (
    <div className="score-details__proof-image centered">
      <img src={url} alt="Image proof" />
    </div>
  );
}
