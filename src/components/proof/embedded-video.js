import React from 'react';

export function getYoutubeEmbedUrl(url) {
  let path = url.match(
    /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|&v(?:i)?=))([^#&?]*).*/,
  )[1];
  return `https://www.youtube.com/embed/${path}`;
}

export function getTwitchEmbedUrl(url) {
  const matches = url.match(/twitch.tv\/(\w+)\/(?:[vc]\/)?(\d+)(\?.+)?/);
  if (matches == null) {
    return null;
  }

  let username = matches[1];
  if (username == null || username === 'videos') {
    username = null;
  }
  let videoId = matches[2];
  let flags = matches[3] ? `&${matches[3]}` : '';

  return `https://player.twitch.tv/?autoplay=false&video=v${videoId}${flags}&parent=localhost&parent=theabsolute.plus`;
}

export default function EmbeddedVideo({ link }) {
  let src;
  const { hostname } = link;

  // TODO: Make this more resilient
  if (hostname.endsWith('twitch.tv')) {
    src = getTwitchEmbedUrl(link.href);
  } else if (hostname.includes('youtu')) {
    src = getYoutubeEmbedUrl(link.href);
  } else if (hostname.includes('streamable')) {
    const shortcode = link.href.match(/streamable.com\/([^/]+)/)?.[1];
    if (shortcode != null) {
      src = `https://streamable.com/o/${shortcode}`;
    }
  } else {
    return (
      <div className="score-details__proof-video">
        <video
          style={{ width: '100%', height: '100%' }}
          controls
          autoPlay=""
          loop=""
          muted=""
        >
          <source src={link.toString()} />
        </video>
      </div>
    );
  }
  return (
    <div className="score-details__proof-video">
      <iframe
        src={src}
        frameBorder="0"
        allow="autoplay; encrypted-media"
        allowFullScreen
        title="video"
      />
    </div>
  );
}
