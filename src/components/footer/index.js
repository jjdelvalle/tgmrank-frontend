import { Trans } from '@lingui/macro';
import React from 'react';
import { Link } from '@reach/router';
import { useLanguage } from '../../contexts/language';
import { ContactInfo } from '../../contact';

import './styles.css';

export default function Footer() {
  const { currentLanguage, changeLanguage } = useLanguage();
  const languages = {
    en: 'English',
    fr: 'Français',
    ja: '日本語',
    de: 'Deutsch',
    pt: 'Português',
    es: 'Español',
  };

  const linkSeparator = (
    <span aria-hidden="true" role="presentation">
      {' '}
      ·{' '}
    </span>
  );

  const footerLinks = [
    <a key="placeholder" href={ContactInfo.discord}>
      Discord
    </a>,
    // <a key="placeholder" href={ContactInfo.mastodon}>
    //   Mastodon
    // </a>,
    <Link key="placeholder" to="/about">
      <Trans>About/Contact</Trans>
    </Link>,
  ];

  const footerElements = footerLinks
    .flatMap((value, index, array) =>
      array.length - 1 !== index // check for the last item
        ? [value, linkSeparator]
        : value,
    )
    .map((element, index) => <span key={index}>{element}</span>);

  return (
    <footer className="footer">
      <ul className="list-bare footer__languages">
        <li>{languages[currentLanguage]}</li>
        {Object.entries(languages)
          .filter(([l]) => l !== currentLanguage)
          .map(([key, title]) => (
            <li key={key}>
              <button className={key} onClick={() => changeLanguage(key)}>
                {title}
              </button>
            </li>
          ))}
      </ul>
      <div className="footer__links">{footerElements}</div>
    </footer>
  );
}
