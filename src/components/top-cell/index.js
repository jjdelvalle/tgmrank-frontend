import React from 'react';
import objstr from 'obj-str';
import Link from '../link';
import { TimeDisplay } from '../numbers';

import './styles.css';

function TopCell({
  videoId,
  rank,
  playerName,
  gradeDisplay,
  score,
  level,
  playtime,
}) {
  return (
    <div
      className={objstr({
        'board-top__cell': true,
      })}
    >
      <img
        className="avatar__thumbnail"
        width="30"
        height="30"
        src={`${process.env.TGMRANK_BASE_URL}/v1/avatar/${playerName}`}
      />
      <Link to={`/player/${playerName}`} className="board-top__name">
        {playerName}
      </Link>
      <div className="board-top__rank">
        {rank === 1 ? '1st' : rank === 2 ? '2nd' : '3rd'} place
      </div>
      <div className="board-top__numbers">
        <div className="number">
          {gradeDisplay !== null ? (
            <>
              <div>{gradeDisplay}</div>
              <div>Grade</div>
            </>
          ) : (
            <>
              <div>{score}</div>
              <div>Score</div>
            </>
          )}
        </div>
        {level && (
          <div className="number">
            <div>{level}</div>
            <div>Level</div>
          </div>
        )}
        <div className="number">
          <div>
            <TimeDisplay time={playtime} />
          </div>
          <div>Time</div>
        </div>
      </div>
      <div className="board-top__links">
        <Link to={`.`}>Details</Link>
        <Link to={`/player/${playerName}`}>Profile</Link>
      </div>
    </div>
  );
}
