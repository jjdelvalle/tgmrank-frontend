import 'regenerator-runtime/runtime';
// This is the browser entry point, it should not be server-side rendered
import React from 'react';
import { render } from 'react-dom';
import Root from './root-dev';

render(<Root />, document.getElementById('root'));
