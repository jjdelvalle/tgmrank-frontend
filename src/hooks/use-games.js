import React, { useContext, useMemo } from 'react';
import TgmRank from '../tgmrank-api';
import slugify from 'slugify-string';
import useSWR from 'swr';

export const ModeTags = {
  Main: 'MAIN',
  Extended: 'EXTENDED',
  Event: 'EVENT',
  Carnival: 'CARNIVAL',
  Doom: 'DOOM',
};

export const RankedModeKeys = {
  main: 'main',
  extended: 'extended',
  secret: 'secret',
  big: 'big',
  world: 'world',
  worldExtended: 'worldExtended',
};

export function leaderboardPath({ game, mode, rankedModeKey } = {}) {
  if (game == null) {
    return '';
  }

  const preferredGameName = game.shortName.toLowerCase();

  if (mode == null) {
    return `/${preferredGameName}${rankedModeKey ? `/${rankedModeKey}` : ''}`;
  }

  const preferredModeName = (
    mode.aliases?.[0] ?? slugify(mode.modeName)
  ).toLowerCase();
  return `/${preferredGameName}/${preferredModeName}`;
}

export function lookupGame(games, gameLookupId) {
  if (games == null || gameLookupId == null) {
    return null;
  }

  if (Number.isInteger(gameLookupId)) {
    return games?.find(g => g.gameId === gameLookupId);
  }

  let gameLookupIdString = gameLookupId?.toLowerCase();
  return games?.find(
    g =>
      g.shortName.toLowerCase() === gameLookupIdString ||
      g.aliases?.includes(gameLookupIdString),
  );
}

export function lookupMode(game, modeLookupId) {
  if (game == null || modeLookupId == null) {
    return null;
  }

  if (Number.isInteger(modeLookupId)) {
    return game?.modes?.find(m => m.modeId === modeLookupId);
  }

  const modeLookupIdString = modeLookupId?.toLowerCase();

  return game?.modes?.find(
    m =>
      slugify(m.modeName.toLowerCase()) === slugify(modeLookupIdString) ||
      m.aliases?.includes(modeLookupIdString),
  );
}

export function lookup(games, gameLookupId, modeLookupId) {
  const game = lookupGame(games, gameLookupId);
  const mode = lookupMode(game, modeLookupId);
  return { game, mode };
}

export function lookupAggregateLeaderboard(game, lookupId) {
  return (
    Object.entries(game.rankedModes).find(
      ([key]) => key.toLowerCase() === lookupId.toLowerCase(),
    ) ?? []
  );
}

function modeIsRecent(mode) {
  const now = Date.now();
  const oneWeekInMs = 604800000;
  if (mode.submissionRange != null) {
    const lowerBoundPass = mode.submissionRange.startMs
      ? now >= mode.submissionRange.startMs - oneWeekInMs
      : true;

    const upperBoundPass = mode.submissionRange.endMs
      ? now <= mode.submissionRange.endMs + oneWeekInMs
      : true;

    return lowerBoundPass && upperBoundPass;
  }
  return true;
}

function modeStarted(mode) {
  return mode.submissionRange != null && mode.submissionRange.startMs != null
    ? Date.now() >= mode.submissionRange.startMs
    : true;
}

function modeIsActive(mode) {
  const now = Date.now();
  if (mode.submissionRange != null) {
    const lowerBoundPass = mode.submissionRange.startMs
      ? now >= mode.submissionRange.startMs
      : true;

    const upperBoundPass = mode.submissionRange.endMs
      ? now <= mode.submissionRange.endMs
      : true;

    return lowerBoundPass && upperBoundPass;
  }
  return true;
}

function addParsedSubmissionRange(mode) {
  if (mode.submissionRange != null) {
    if (mode.submissionRange.start != null) {
      mode.submissionRange.startMs = Date.parse(mode.submissionRange.start);
    }

    if (mode.submissionRange.end != null) {
      mode.submissionRange.endMs = Date.parse(mode.submissionRange.end);
    }
  }

  return mode;
}

function addModeAliases(modeList, modeId, aliasList) {
  const mode = modeList.find(m => m.modeId === modeId);
  if (mode != null) {
    mode.aliases = aliasList;
  }
}

export function enhanceGameModel(games) {
  games.lookup = function (gameLookupId, modeLookupId) {
    return lookup(this, gameLookupId, modeLookupId);
  };

  games.lookupMode = function (modeId) {
    for (let g = 0; g < games.length; g++) {
      for (let m = 0; m < games[g]?.modes?.length; m++) {
        if (games[g].modes[m].modeId === modeId) {
          return games[g].modes[m];
        }
      }
    }

    return null;
  };

  // This isn't completely awful, is it?
  games[1] = games.tgm1 = {
    ...games.find(g => g.gameId === 1),
    aliases: ['tgm1', 'tgm'],
    rankedModes: {
      main: [1, 2],
      extended: [1, 2, 3, 4, 29],
    },
    wikiLink: 'https://tetris.wiki/Tetris_The_Grand_Master',
    twitchLink:
      'https://www.twitch.tv/directory/game/Tetris%3A%20The%20Grand%20Master',
  };

  games[2] = games.tap = {
    ...games.find(g => g.gameId === 2),
    aliases: ['tgm2', 'tgm2p', 'tgm2+', 'tap'],
    rankedModes: {
      main: [6, 7],
      extended: [12, 6, 7, 13, 11, 9, 14, 8],
    },
    wikiLink: 'https://tetris.wiki/Tetris_The_Absolute_The_Grand_Master_2',
    twitchLink:
      'https://www.twitch.tv/directory/game/Tetris%20the%20Absolute%3A%20The%20Grand%20Master%202',
  };

  games[3] = games.ti = {
    ...games.find(g => g.gameId === 3),
    aliases: ['tgm3', 'ti'],
    rankedModes: {
      main: [15, 16],
      extended: [15, 16, 17, 18, 25, 19, 30],
      world: [20, 21],
      worldExtended: [20, 21, 22, 23, 24, 26, 31],
    },
    wikiLink: 'https://tetris.wiki/Tetris_The_Grand_Master_3_Terror-Instinct',
    twitchLink:
      'https://www.twitch.tv/directory/game/Tetris%3A%20The%20Grand%20Master%203%20-%20Terror%E2%80%91Instinct',
  };

  games[0] = games.overall = {
    ...games.find(g => g.gameId === 0),
    aliases: ['all', 'overall'],
    rankedModes: {
      main: games.tgm1.rankedModes.main.concat(
        games.tap.rankedModes.main,
        games.ti.rankedModes.main,
      ),
      extended: games.tgm1.rankedModes.extended.concat(
        games.tap.rankedModes.extended,
        games.ti.rankedModes.extended,
      ),
      secret: [5, 14, 19],
      big: [3, 10, 25],
    },
  };

  const allRankedModes = Object.values(games.overall.rankedModes)
    .concat(
      Object.values(games.tgm1.rankedModes),
      Object.values(games.tap.rankedModes),
      Object.values(games.ti.rankedModes),
    )
    .flat();

  for (let g = 0; g < games.length; g++) {
    games[g].indexedModes = {};
    games[g].lookupRankedMode = function (lookupId) {
      return lookupAggregateLeaderboard(this, lookupId);
    };

    for (let m = 0; m < games[g]?.modes?.length; m++) {
      games[g].indexedModes[games[g].modes[m].modeId] = games[g].modes[m];

      games[g].modes[m].game = games[g];
      games[g].modes[m].isRanked = allRankedModes.includes(
        games[g].modes[m].modeId,
      );

      addParsedSubmissionRange(games[g].modes[m]);

      games[g].modes[m].isActive = function () {
        return modeIsActive(this);
      };
      games[g].modes[m].hasStarted = function () {
        return modeStarted(this);
      };
      games[g].modes[m].isRecent = function () {
        return modeIsRecent(this);
      };
    }
  }

  games.tap.modes.find(m => m.modeId === 8).links = [
    {
      title: 'Death Series of 5 Calculator',
      link: '/tools/death',
    },
  ];

  games.tap.modes
    .filter(m => m?.tags?.includes(ModeTags.Carnival))
    .forEach(
      m =>
        (m.links = [
          {
            title: 'Carnival of Death (tetris.wiki)',
            link: 'https://tetris.wiki/Carnival_of_Death',
          },
        ]),
    );

  // From 2017: https://tetris.wiki/Carnival_of_Death
  const carnival2021 = games.tap.modes.find(
    m => m.modeName === 'Carnival of Death 2021',
  );
  if (carnival2021 != null) {
    carnival2021.carnivalPb = 42045;
  }

  const carnival2022 = games.tap.modes.find(
    m => m.modeName === 'Carnival of Death 2022',
  );
  if (carnival2022 != null) {
    carnival2022.carnivalPb = 52447;
  }

  addModeAliases(games.tap.modes, 8, ['death-series', 'death-series-of-5']);
  addModeAliases(games.tap.modes, 13, ['tgm-plus']);
  addModeAliases(games.tap.modes, 41, ['tgm-plus-secret-grade']);

  addModeAliases(games.ti.modes, 15, ['master', 'master-classic']);
  addModeAliases(games.ti.modes, 30, [
    'qualified-master',
    'qualified-master-classic',
  ]);
  addModeAliases(games.ti.modes, 16, ['shirase', 'shirase-classic']);
  addModeAliases(games.ti.modes, 17, ['easy', 'easy-classic']);
  addModeAliases(games.ti.modes, 18, ['sakura', 'sakura-classic']);
  addModeAliases(games.ti.modes, 19, [
    'master-secret-grade',
    'master-secret-grade-classic',
  ]);
  addModeAliases(games.ti.modes, 27, [
    'shirase-secret-grade',
    'shirase-secret-grade-classic',
  ]);
  addModeAliases(games.ti.modes, 25, ['big', 'big-classic']);
  addModeAliases(games.ti.modes, 20, ['master-world']);
  addModeAliases(games.ti.modes, 31, ['qualified-master-world']);
  addModeAliases(games.ti.modes, 21, ['shirase-world']);
  addModeAliases(games.ti.modes, 22, ['easy-world']);
  addModeAliases(games.ti.modes, 23, ['sakura-world']);
  addModeAliases(games.ti.modes, 24, ['master-secret-grade-world']);
  addModeAliases(games.ti.modes, 28, ['shirase-secret-grade-world']);
  addModeAliases(games.ti.modes, 26, ['big-world']);

  return games;
}

const GamesContext = React.createContext();

export function GamesProvider({ children }) {
  const { data: gameData, error } = useSWR(TgmRank.getGamesUrl(), {
    refreshInterval: 30000,
    focusThrottleInterval: 30000,
    dedupingInterval: 30000,
  });

  const enhancedGameData = useMemo(() => {
    return gameData != null ? enhanceGameModel(gameData) : null;
  }, [gameData]);

  return (
    <GamesContext.Provider
      value={{
        games: enhancedGameData,
        error,
      }}
    >
      {children}
    </GamesContext.Provider>
  );
}

export function useGames() {
  return useContext(GamesContext);
}
