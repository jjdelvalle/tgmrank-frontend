import { useEffect } from 'react';
import { isNotBanned, useUser } from '../contexts/user';
import { navigate } from '@reach/router';

export function useAuth(validator = isNotBanned, redirectTo = '/login') {
  const { verifyLogin } = useUser();

  useEffect(() => {
    async function redirectIfLoggedOut() {
      const user = await verifyLogin();
      if (user.error || !validator(user)) {
        await navigate(redirectTo, { replace: true });
      }
    }

    redirectIfLoggedOut();
  }, []);
}
