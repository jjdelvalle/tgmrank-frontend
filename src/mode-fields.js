export const ModeFieldType = {
  Required: 'required',
  Optional: 'optional',
  Hidden: 'hidden',
};

export function isRequired(modeField) {
  return modeField === ModeFieldType.Required;
}

export function isOptional(modeField) {
  return modeField === ModeFieldType.Optional;
}

export function mustBeEmpty(modeField) {
  return modeField === ModeFieldType.Hidden;
}
