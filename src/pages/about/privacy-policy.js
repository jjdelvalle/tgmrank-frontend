import React from 'react';
import { Helmet } from 'react-helmet-async';
import LongDate from '../../components/date';
import Layout from '../../components/layout';

export default function PrivacyPolicyPage() {
  return (
    <Layout>
      <Helmet>
        <title>Privacy Policy</title>
      </Helmet>
      <div className="privacy-policy">
        <h1>Privacy Policy</h1>
        <p>
          Last Updated On:{' '}
          <strong>
            <LongDate>2021-07-03T15:28:23Z</LongDate>
          </strong>
        </p>
        <h2>What data do we collect?</h2>
        <p>This site collects the following data:</p>
        <ul>
          <li>Email address</li>
          <li>Username</li>
          <li>
            Password hashes (Passwords are <strong>not</strong> stored in
            plain-text)
          </li>
          <li>User settings</li>
          <li>User-submitted scores and links to score proof</li>
        </ul>
        <h2>How do we collect your data?</h2>
        <p>
          The user directly provides most of the data we collect. We collect and
          process data when users:
        </p>
        <ul>
          <li>Create an account</li>
          <li>Update their user settings</li>
          <li>Submit their scores</li>
        </ul>
        <p>
          Additionally, in the interest of maintaining leaderboard integrity,
          site administrators can:
        </p>
        <ul>
          <li>
            Grant people access to their imported legacy accounts with an email
            address
          </li>
          <li>Approve or reject submitted scores</li>
          <li>Ban users</li>
          <li>
            In rare circumstances, add scores and score proof on the behalf of
            others
          </li>
          <li>Create backups of users&apos; publicly posted score proof</li>
        </ul>
        <h2>What log data do we collect?</h2>
        <p>
          Like most websites, theabsolute.plus collects information that your
          browser sends whenever you visit the website. This data may include
          internet protocol (IP) addresses, browser type, Internet Service
          Provider (ISP), date and time stamp, and referring/exit pages. As of
          now, we only use this information to debug site issues. No log data is
          shared with third-party services.
        </p>
        <h2>What are cookies?</h2>
        <p>
          Cookies are text files placed on your computer to collect standard
          Internet log information and visitor behavior information. When you
          visit our websites, we may collect information from you automatically
          through cookies or similar technology.
        </p>
        <p>
          For further information, visit{' '}
          <a href="https://www.wikipedia.org/wiki/HTTP_cookie">
            HTTP cookie on Wikipedia
          </a>
        </p>
        <h2>How do we use cookies?</h2>
        <p>theabsolute.plus uses cookies to:</p>
        <ul>
          <li>Keep you signed in</li>
          <li>
            Remember a few site settings, like language preference and
            light/dark theme preference
          </li>
        </ul>
        <h2>How to manage cookies</h2>
        <p>
          You can set your browser not to accept cookies. However some of our
          website features may behave unexpectedly or fail to function as a
          result.
        </p>
        <h2>Other stored data</h2>
        <p>
          In addition to cookies, theabsolute.plus also stores other site data
          on users&apos; devices in order to:
        </p>
        <ul>
          <li>
            Keep track of shorter-lived settings, like leaderboard filters
          </li>
          <li>
            Keep track incomplete forms so that users can return to them
            pre-filled
          </li>
        </ul>
        <h2>External Privacy Policies</h2>
        <p>
          theabsolute.plus links to external websites, particularly for
          user-submitted score proof. Our privacy policy only applies to our
          website, so if you click on a link to another website, you should read
          their privacy policy.
        </p>
        <p>Some common sites we link to</p>
        <ul>
          <li>
            <a href="https://imgur.com">Imgur</a> (
            <a href="https://imgurinc.com/privacy">Privacy Policy</a>)
          </li>
          <li>
            <a href="https://discord.com/">Discord</a> (
            <a href="https://discord.com/privacy">Privacy Policy</a>)
          </li>
          <li>
            <a href="https://www.twitch.tv/">Twitch</a> (
            <a href="https://www.twitch.tv/p/en/legal/privacy-notice/">
              Privacy Policy
            </a>
            )
          </li>
          <li>
            <a href="https://www.youtube.com">Youtube</a> (
            <a href="https://policies.google.com/privacy?hl=en">
              Privacy Policy
            </a>
            )
          </li>
        </ul>
        <h2>Data Deletion Requests</h2>
        <p>
          If you do not want your data to listed on this site anymore, please
          contact an admin. Contact information can be found on the{' '}
          <a href="/about">about</a> page.
        </p>
        <h2>How to contact us</h2>
        <p>
          If you have any questions about theabsolute.plus&apos; privacy policy
          or any other privacy concern, contact information can be found on the{' '}
          <a href="/about">about</a> page.
        </p>
      </div>
    </Layout>
  );
}
