import React from 'react';
import { Helmet } from 'react-helmet-async';
import Layout from '../../components/layout';
import { ContactInfo } from '../../contact';

import './styles.css';

export function AboutSnippet() {
  return (
    <div className="about">
      <p>
        <em>Tetris the Grandmaster (TGM)</em> is a puzzle game series by{' '}
        <a href="http://www.arika.co.jp/">Arika</a> based on{' '}
        <a href="https://www.youtube.com/watch?v=XkU_3nzcMkQ">Sega Tetris</a>.
        Hallmarks of the series include{' '}
        <a href="https://www.youtube.com/watch?v=tPDYNuldc-4">fast gameplay</a>,{' '}
        <a href="https://youtu.be/DaXPMcx2g0I?t=38m35s">instant gravity</a>, and{' '}
        <a href="https://youtu.be/eU8UNDVZNQY?t=5m13s">invisible pieces</a>.
        It&#39;s fun. It&#39;s been featured at Games Done Quick events a{' '}
        <a href="https://youtu.be/_oqRCaDeeGg?t=1204">handful</a> of{' '}
        <a href="https://www.youtube.com/watch?v=DaXPMcx2g0I">times</a>.
      </p>
      <p>
        This site is a repository of scores and records held across the TGM
        community; a place to compare scores and inspire competition.
      </p>
    </div>
  );
}

export default function AboutPage() {
  return (
    <Layout>
      <Helmet>
        <title>About/Contact</title>
      </Helmet>
      <div className="about">
        <h2>About TGM Rank</h2>
        <AboutSnippet />
        <ul>
          <li>
            <a href="/about/privacy">Privacy Policy</a>
          </li>
          <li>
            <a href="/about/proof">Proof Policy</a>
          </li>
        </ul>
        <h3>Links</h3>
        <ul>
          <li>
            <a href={`${ContactInfo.discord}`}>Discord</a>
          </li>
          {/*<li>*/}
          {/*  <a href={`${ContactInfo.mastodon}`}>Mastodon</a>*/}
          {/*</li>*/}
          <li>
            <a href="https://twitter.com/tgm_series">@tgm_series</a> /{' '}
            <a href="https://twitter.com/hashtag/tgm_series?src=hash">
              #tgm_series
            </a>
          </li>
          <li>
            <a href="https://tetrisconcept.net/">TetrisConcept</a>
          </li>
          <li>
            <a href="http://pier21.sakura.ne.jp/pier/">Pier21</a>
          </li>
          <li>
            <s>
              <a href="http://pier21.sakura.ne.jp/tetrisboard">
                Pier21 Tetris Ranking
              </a>
            </s>{' '}
            (RIP)
          </li>
          <li>
            <s>
              <a href="http://www7a.biglobe.ne.jp/~tetris_holic/ranking_frame.htm">
                TETRiS-HOLiC Leaderboards
              </a>
            </s>{' '}
            (RIP)
          </li>
          <li>
            <a href="http://tgm.tips/">TGM Guide</a>
          </li>
          <li>
            <a href="https://tetris.wiki/">Tetris.wiki</a>
          </li>
        </ul>
        <h3>Contact</h3>
        <ul>
          <li>
            <a href={ContactInfo.discord}>#leaderboard-dev in Discord</a>
          </li>
          <li>
            <a href={`mailto:${ContactInfo.email}`}>Email</a>
          </li>
          <li>
            <a href="https://gitlab.com/groups/tgm-rank/-/issues">
              Roadmap/Issue Tracker
            </a>
          </li>
        </ul>
      </div>
    </Layout>
  );
}
