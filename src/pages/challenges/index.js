import { Trans } from '@lingui/macro';
import Layout from '../../components/layout';
import { Helmet } from 'react-helmet-async';
import React from 'react';
import { Progress } from '../../components/progress';
import Tag from '../../components/tag';
import { leaderboardPath, ModeTags, useGames } from '../../hooks/use-games';
import Link from '../../components/link';
import useSWR from 'swr';
import TgmRank from '../../tgmrank-api';
import { useLeaderboardFilter } from '../../components/leaderboard-header/filter';

function ModeList({ modes }) {
  return (
    <ul>
      {modes?.length > 0 ? (
        modes.map(mode => (
          <li key={mode.modeId}>
            <Link to={leaderboardPath({ game: mode.game, mode })}>
              {mode.modeName}
            </Link>
          </li>
        ))
      ) : (
        <li className="dim">None</li>
      )}
    </ul>
  );
}

export function EventStatusTag({ mode }) {
  if (mode.isActive() && mode.hasStarted()) {
    return <Tag color="green">Open</Tag>;
  } else if (!mode.isActive() && mode.hasStarted()) {
    return <Tag color="red">Closed</Tag>;
  }
  return <Tag color="blue">Upcoming</Tag>;
}

export function CarnivalTheme() {
  return (
    <Helmet>
      <body className="carnival-theme" />
    </Helmet>
  );
}

function DeathQuote() {
  const quotes = [
    <q key={1}>
      <span className="highlight">Death</span> may be the greatest of all human
      blessings.
    </q>,
    <q key={2}>
      I must not fear. Fear is the mind-killer. Fear is{' '}
      <span className="highlight">[The Absolute] Death</span> that brings total
      obliteration.
    </q>,
    <q key={3}>
      While I thought that I was learning how to live, I have been learning how
      to <span className="highlight">die</span>.
    </q>,
    <q key={4}>
      To the well-organised mind, <span className="highlight">Death</span> is
      but the next great adventure.
    </q>,
    <q key={5}>
      The fear of <span className="highlight">death</span> follows from the fear
      of life. A man who lives fully is prepared to{' '}
      <span className="highlight">die</span> at any time.
    </q>,
    <q key={6}>
      You only live twice: Once when you are born, and once when you look{' '}
      <span className="highlight">death</span> in the face
    </q>,
    <q key={7}>
      <span className="highlight">Death</span> is not the opposite of life, but
      a part of it.
    </q>,
    <q key={8}>
      One should <span className="highlight">die</span> proudly when it is no
      longer possible to live proudly.
    </q>,
    <q key={9}>
      <span className="highlight">Death</span> is Peaceful, Life is Harder
    </q>,
    <q key={9}>
      Life is pleasant. <span className="highlight">Death</span> is peaceful.
      It&#39;s the transition that&#39;s troublesome.
    </q>,
  ];

  return quotes[Math.floor(Math.random() * quotes.length)];
}

export function DeathToll({ mode }) {
  const { filter } = useLeaderboardFilter();

  const { data: ranking, error } = useSWR(
    mode != null
      ? TgmRank.getModeRankingUrl(mode.modeId, {
          asOf: filter.asOf,
          locationFilter: filter.locations,
        }).toString()
      : null,
  );

  const total = ranking?.reduce((acc, item) => acc + item.level, 0);
  const completionPercentage = (total / mode.carnivalPb) * 100;

  return (
    <>
      <div className="quote">
        <DeathQuote />
      </div>
      {total > 0 && (
        <div>
          <div className="death-toll font-effect-fire-animation">
            <Trans>DEATH TOLL</Trans>: <span>{total}</span>
          </div>
          {mode.carnivalPb && (
            <div>
              <Progress percent={completionPercentage} />
              <div className="font-effect-fire-animation">
                Goal: {mode.carnivalPb} ({completionPercentage.toFixed(2)}%)
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
}

export function useCountdown(init) {
  function calculateTime() {
    return Math.max(0, (init ?? 0) - Date.now());
  }

  function toTimeComponents(ms) {
    let milliseconds = Math.floor(ms % 1000),
      seconds = Math.floor((ms / 1000) % 60),
      minutes = Math.floor((ms / (1000 * 60)) % 60),
      hours = Math.floor((ms / (1000 * 60 * 60)) % 24),
      days = Math.floor(ms / (1000 * 60 * 60 * 24));

    return {
      milliseconds,
      seconds,
      minutes,
      hours,
      days,
      raw: ms,
    };
  }

  const [timer, setTimer] = React.useState(calculateTime());

  React.useEffect(() => {
    if (timer > 0) {
      setTimeout(() => setTimer(calculateTime()), 50);
    }
  }, [timer]);

  return toTimeComponents(timer);
}

export function countdownBlinkerStyle(timer) {
  return { animation: `blink-red ${(timer.days + 1) / 2}s infinite` };
}

export function Countdown({ timer }) {
  return (
    <>
      <span>
        {timer.days.toString().padStart(2, '0')}:
        {timer.hours.toString().padStart(2, '0')}:
        {timer.minutes.toString().padStart(2, '0')}:
        {timer.seconds.toString().padStart(2, '0')}.
        {timer.milliseconds.toString().padStart(3, '0')}
      </span>
    </>
  );
}

export function DoomsdayCountdown({ mode }) {
  const timer = useCountdown(mode.submissionRange?.endMs ?? 0);

  return (
    <>
      {timer.raw <= 0 && (
        <iframe
          width="560"
          height="315"
          src="https://www.youtube-nocookie.com/embed/9wGRK6uFx1k?start=17&autoplay=1"
          title="YouTube video player"
          frameBorder="0"
          allow="autoplay; encrypted-media; picture-in-picture"
          allowFullScreen
        />
      )}
      <div className="doomsday-countdown">
        Doomsday Countdown:{' '}
        <span style={countdownBlinkerStyle(timer)}>
          <Countdown timer={timer} />
        </span>
      </div>
    </>
  );
}

export default function ChallengesPage() {
  const { games } = useGames();

  const eventModes = games
    ?.flatMap(g => g.modes)
    ?.filter(m => m?.tags?.includes(ModeTags.Event) === true)
    ?.sort((a, b) =>
      b.submissionRange?.start?.localeCompare(a.submissionRange?.start),
    );

  // const mode = eventModes?.find(m => slugify(m.modeName) === modeName);

  const futureModes = eventModes?.filter(m => !m.hasStarted());
  const activeModes = eventModes?.filter(m => m.isActive() && m.hasStarted());
  const inactiveModes = eventModes?.filter(
    m => !m.isActive() && m.hasStarted(),
  );

  return (
    <Layout>
      <Helmet>
        <title>Challenges</title>
      </Helmet>
      <div>
        <h2>
          <Trans>Events</Trans>
        </h2>
        {activeModes?.length > 0 && (
          <>
            <h3>
              <Trans>Active</Trans>
            </h3>
            <ModeList modes={activeModes} />
          </>
        )}

        {futureModes?.length > 0 && (
          <>
            <h3>
              <Trans>Upcoming</Trans>
            </h3>
            <ModeList modes={futureModes} />
          </>
        )}

        {inactiveModes?.length > 0 && (
          <>
            <h3>
              <Trans>Archive</Trans>
            </h3>
            <ModeList modes={inactiveModes} />
          </>
        )}
      </div>
    </Layout>
  );
}
