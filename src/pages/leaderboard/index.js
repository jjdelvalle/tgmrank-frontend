import React from 'react';
import { Helmet } from 'react-helmet-async';
import useSWR from 'swr';
import { LongDateTime } from '../../components/date';
import { toTitleCase } from '../../components/string-utilities';
import Tag, { TagGroup } from '../../components/tag';

import { notFoundRedirect } from '../404';
import Layout from '../../components/layout';
import Leaderboard from '../../components/leaderboard';
import LeaderboardHeader, {
  LeaderboardNavigation,
  ModeDescription,
} from '../../components/leaderboard-header';
import { Trans } from '@lingui/macro';
import { leaderboardPath, ModeTags, useGames } from '../../hooks/use-games';
import TgmRank, { RankingAlgorithm } from '../../tgmrank-api';
import { useLeaderboardFilter } from '../../components/leaderboard-header/filter';
import {
  countdownBlinkerStyle,
  CarnivalTheme,
  Countdown,
  DeathToll,
  DoomsdayCountdown,
  EventStatusTag,
  useCountdown,
} from '../challenges';

import './styles.css';
import ModeName from '../../components/mode-name';
import Link from '../../components/link';
import {
  AggregateLeaderboardDescription,
  AggregateLeaderboardLabel,
} from '../../components/leaderboard/label';

import Cell from '../../components/cell';

const defaultLeaderboard = 'main';

function getRankingAlgorithm(filter) {
  return filter.experimentalRankings;
}

function EventCountdown({ endMs }) {
  const timer = useCountdown(endMs);

  return (
    timer.raw > 0 && (
      <span style={countdownBlinkerStyle(timer)}>
        <span>Ends in: </span>
        <Countdown timer={timer} />
      </span>
    )
  );
}

export default function LeaderboardPage({ game, leaderboard }) {
  leaderboard = leaderboard ?? defaultLeaderboard;

  const { games } = useGames();

  if (games == null) {
    return null;
  }

  const { game: currentGame, mode: currentMode } =
    games.lookup?.(game, leaderboard) ?? {};

  const [rankedModeKey] = currentGame?.lookupRankedMode(leaderboard) ?? [];
  const isModeLeaderboard = currentMode != null;

  if (!currentGame || (!currentMode && rankedModeKey == null)) {
    notFoundRedirect('We could not find the leaderboard you were looking for.');
  }

  let title;
  if (currentMode) {
    title = `${currentGame.shortName}: ${currentMode.modeName} rankings`;
  } else if (currentGame) {
    title = `${currentGame.gameName} rankings`;
  }

  return (
    <>
      <Helmet>
        <title>{title}</title>
      </Helmet>
      <Layout>
        <LeaderboardHeader
          currentGame={currentGame}
          currentMode={currentMode}
        />
        <LeaderboardNavigation game={currentGame} />
        {currentGame != null && (
          <>
            <div className="leaderboard-title">
              <Link
                to={leaderboardPath({
                  game: currentGame,
                  mode: currentMode,
                  rankedModeKey,
                })}
              >
                {currentGame.gameName === 'Overall' ? (
                  <Trans>Overall</Trans>
                ) : (
                  currentGame.shortName
                )}
                {' - '}
                {isModeLeaderboard ? (
                  <>
                    <ModeName
                      modeId={currentMode.modeId}
                      modeName={currentMode.modeName}
                      includeSuffix={false}
                    />
                  </>
                ) : (
                  <>
                    <AggregateLeaderboardLabel
                      game={currentGame}
                      rankedModeKey={rankedModeKey}
                      showDescriptionButton={false}
                    />
                  </>
                )}
              </Link>
            </div>
            <TagGroup>
              {currentMode?.tags && <EventStatusTag mode={currentMode} />}

              {currentMode?.tags?.map((tag, tagIndex) => (
                <Tag key={tagIndex}>{toTitleCase(tag)}</Tag>
              ))}
              {currentMode?.submissionRange != null && (
                <>
                  <Tag>
                    <span>Start: </span>
                    {currentMode.submissionRange.start && (
                      <LongDateTime>
                        {currentMode.submissionRange.start}
                      </LongDateTime>
                    )}
                  </Tag>
                  <Tag>
                    <span>End: </span>
                    {currentMode.submissionRange.end && (
                      <LongDateTime>
                        {currentMode.submissionRange.end}
                      </LongDateTime>
                    )}
                  </Tag>
                  {currentMode?.isActive() && (
                    <Tag>
                      <EventCountdown
                        endMs={currentMode.submissionRange.endMs}
                      />
                    </Tag>
                  )}
                </>
              )}
            </TagGroup>
          </>
        )}
        {currentMode != null && isModeLeaderboard && (
          <div className="leaderboard-description">
            <ModeDescription modeId={currentMode.modeId} showDefault={false} />
          </div>
        )}
        {currentGame != null && !isModeLeaderboard && (
          <AggregateLeaderboardDescription
            game={currentGame}
            rankedModeKey={rankedModeKey}
          />
        )}
        {currentMode?.tags?.includes(ModeTags.Carnival) && (
          <>
            <CarnivalTheme />
            <DeathToll mode={currentMode} />
          </>
        )}
        {currentMode?.tags?.includes(ModeTags.Doom) && (
          <>
            <DoomsdayCountdown mode={currentMode} />
          </>
        )}
        {isModeLeaderboard
          ? currentMode && <ModeLeaderboard mode={currentMode} />
          : currentGame && (
              <GameLeaderboard
                game={currentGame}
                rankedModeKey={rankedModeKey}
              />
            )}
      </Layout>
    </>
  );
}

export function ModeLeaderboard({ mode }) {
  const { filter } = useLeaderboardFilter();

  const { data: ranking, error } = useSWR(
    TgmRank.getModeRankingUrl(mode.modeId, {
      isMainMode: mode.game.rankedModes.main.includes(mode.modeId),
      asOf: filter.asOf,
      locationFilter: filter.locations,
      algorithm: getRankingAlgorithm(filter),
    }).toString(),
  );

  if (error) {
    return <div>{`Error! ${error}`}</div>;
  }

  if (ranking) return <Leaderboard scores={ranking} mode={mode} />;
  else return <div />;
}

export function GameLeaderboard({ game, rankedModeKey }) {
  const { filter } = useLeaderboardFilter();
  const modeIds = game?.rankedModes?.[rankedModeKey];

  const { data: ranking, error } = useSWR(
    TgmRank.getAggregatedRankingUrl(game.gameId, {
      asOf: filter.asOf,
      rankedModeKey,
      modeIds,
      locationFilter: filter.locations,
      algorithm: getRankingAlgorithm(filter),
    }).toString(),
  );

  if (error) {
    return <div>{`Error! ${error}`}</div>;
  }

  if (ranking)
    return (
      <Leaderboard scores={ranking} game={game} rankedModeKey={rankedModeKey} />
    );
  else return <div />;
}

export function FightcadeLeaderboardPage() {
  const { games } = useGames();
  const { game } = games?.lookup?.(2) ?? {};

  const { data: ranking, error } = useSWR(
    TgmRank.getFightcadeRankingUrl().toString(),
  );

  if (game == null) {
    return null;
  }

  const stats = [
    {
      title: 'Ranked Matches',
      stat: score => score.gameinfo.tgm2p.num_matches,
    },
    {
      title: 'Time Played',
      stat: score => {
        const totalSeconds = Number(score.gameinfo.tgm2p.time_played);
        var hours = Math.floor(totalSeconds / 3600);
        var minutes = Math.floor((totalSeconds % 3600) / 60);
        return `${hours
          .toString()
          .padStart(2, '0')}h ${minutes.toString().padStart(2, '0')}m`;
      },
    },
  ];

  return (
    <>
      <Helmet>
        <title>Fightcade Versus Leaderboard</title>
      </Helmet>
      <Layout>
        <LeaderboardHeader currentGame={game} />
        <LeaderboardNavigation game={game} />

        {ranking && (
          <div className="leaderboard">
            {ranking.map((item, index) => (
              <Cell key={index} stats={stats} score={item} />
            ))}
          </div>
        )}
      </Layout>
    </>
  );
}
