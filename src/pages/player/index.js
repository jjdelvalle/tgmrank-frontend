import React from 'react';
import { Helmet } from 'react-helmet-async';
import useSWR from 'swr';
import { Trans } from '@lingui/macro';
import NavLink from '../../components/nav-link';
import Layout from '../../components/layout';
import PlayerScores from '../../components/scores';
import RecentActivity from '../../components/recent-activity';
import { MAX_RIVALS, RivalTable } from '../../components/rival';
import TgmRank, { ScoreStatus } from '../../tgmrank-api';

import { notFoundRedirect } from '../404';

import './styles.css';
import PlayerPageLayout from '../../components/player-page-layout';
import { useGames } from '../../hooks/use-games';
import { PlayerRanking } from '../../components/player-bio';
import { AggregateLeaderboardLabel } from '../../components/leaderboard/label';
import { GameLink } from '../../components/mode-name';
import PlayerAchievements from '../../components/achievement';

export function GameRanking({ playerId, game }) {
  if (game == null || playerId == null) {
    return null;
  }

  return (
    <>
      <div className="player__game-ranking">
        {Object.keys(game.rankedModes).map((key, i) => {
          return (
            <GameLink key={i} game={game} rankedModeKey={key}>
              <PlayerRanking
                playerId={playerId}
                rankedModeKey={key}
                game={game}
                title={
                  <AggregateLeaderboardLabel
                    game={game}
                    rankedModeKey={key}
                    showDescriptionButton={false}
                  />
                }
              />
            </GameLink>
          );
        })}
      </div>
    </>
  );
}

export default function PlayerPage({ playerName, tab }) {
  const { data: playerData, error } = useSWR(
    playerName ? TgmRank.getPlayerByName(playerName) : null,
  );

  const { data: playerScores } = useSWR(
    playerData ? TgmRank.getPlayerScoresUrl(playerData.playerId) : null,
  );

  const { data: rivalData } = useSWR(() =>
    TgmRank.getRivalsUrl(playerData.playerId),
  );

  const { games } = useGames();
  if (games == null) {
    return null;
  }

  // TODO: Use nested routes?
  const { game } = games?.lookup?.(tab?.toLowerCase());
  const isRivalsTab = tab?.toLowerCase() === 'rivals';
  const isAchievementsTab = tab?.toLowerCase() === 'achievements';
  const isOverviewPage = tab == null;
  if (
    games?.length > 0 &&
    game == null &&
    !isRivalsTab &&
    !isOverviewPage &&
    !isAchievementsTab
  ) {
    notFoundRedirect('We could not find the page you were looking for.');
  }

  function filterScores(scores, excludedStatuses) {
    return scores?.filter(s => !excludedStatuses.includes(s.status));
  }

  if (error) {
    notFoundRedirect();
  }

  const filteredScores = filterScores(playerScores, [
    ScoreStatus.Rejected,
  ])?.filter(s => games.lookup?.(s.gameId, s.modeId).mode?.isActive());
  const groupedScores = filteredScores?.reduce(
    (acc, score) => {
      if (acc.games[score.gameId] == null) {
        acc.games[score.gameId] = [];
      }
      acc.games[score.gameId].push(score);

      if (acc.modes[score.modeId] == null) {
        acc.modes[score.modeId] = [];
      }
      acc.modes[score.modeId].push(score);

      return acc;
    },
    { games: [], modes: [] },
  );

  const tabName = () =>
    isRivalsTab
      ? 'Rivals'
      : isAchievementsTab
      ? 'Achievements'
      : isOverviewPage
      ? 'Overview'
      : game?.shortName;

  return (
    <Layout>
      <>
        <Helmet>
          <title>
            {playerData
              ? `${playerData.playerName} / ${tabName()}`
              : `${playerName}`}
          </title>
        </Helmet>
        <PlayerPageLayout playerName={playerName}>
          <div className="player-scores">
            <ul className="player-scores__tabs">
              <li>
                <NavLink
                  data-bold-toggle="Overview"
                  to={`/player/${playerName}`}
                >
                  <Trans>Overview</Trans>
                </NavLink>
              </li>
              {games?.length > 0 &&
                groupedScores &&
                Object.entries(groupedScores.games)
                  .filter(([, scores]) => scores.length > 0)
                  .map(([gameId, scores], index) => (
                    <li key={index}>
                      <NavLink
                        to={`/player/${playerName}/${games[gameId].shortName}`}
                      >
                        <div data-bold-toggle={games[gameId].shortName}>
                          {games[gameId].shortName}
                        </div>{' '}
                        <div className="badge">{scores.length}</div>
                      </NavLink>
                    </li>
                  ))}
              <li>
                <NavLink
                  data-bold-toggle="Achievements"
                  to={`/player/${playerName}/achievements`}
                >
                  <Trans>Achievements</Trans>
                </NavLink>
              </li>
              <li>
                <NavLink
                  data-bold-toggle="Rivals"
                  to={`/player/${playerName}/rivals`}
                >
                  <Trans>Rivals</Trans>{' '}
                  <div className="badge">
                    {rivalData?.length}/{MAX_RIVALS}
                  </div>
                </NavLink>
              </li>
            </ul>
            <div className="player-scores__grid">
              {isRivalsTab && playerData?.playerId != null && (
                <RivalTable playerData={playerData} rivals={rivalData} />
              )}
              {isAchievementsTab && playerData?.playerId != null && (
                <PlayerAchievements playerId={playerData.playerId} />
              )}
              {game != null && (
                <>
                  <h3>
                    <GameLink game={game}>{game.shortName}</GameLink>
                  </h3>
                  <GameRanking playerId={playerData?.playerId} game={game} />
                  {game?.modes
                    .filter(m => groupedScores?.modes[m.modeId] != null)
                    .map(mode => (
                      <PlayerScores
                        key={mode.modeId}
                        game={game}
                        mode={mode}
                        scores={groupedScores.modes[mode.modeId]}
                      />
                    ))}
                </>
              )}

              {isOverviewPage && playerData?.playerName != null && (
                <RecentActivity playerName={playerData?.playerName} />
              )}
            </div>
          </div>
        </PlayerPageLayout>
      </>
    </Layout>
  );
}
