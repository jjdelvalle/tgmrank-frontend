import React, { createRef, useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import {
  AlertDialog,
  AlertDialogDescription,
  AlertDialogLabel,
} from '@reach/alert-dialog';
import { t, Trans } from '@lingui/macro';
import { useLingui } from '@lingui/react';
import Layout from '../../components/layout';
import Link from '../../components/link';
import { ModeTags, useGames } from '../../hooks/use-games';
import TgmRank, { ProofType, ScoreStatus } from '../../tgmrank-api';

import './styles.css';
import '../../components/select/style.css';
import { useUser, isAdmin } from '../../contexts/user';
import {
  GradeTitle,
  LevelTitle,
  ScoreTitle,
  TimeTitle,
} from '../../components/score';
import { useAuth } from '../../hooks/use-auth';
import { isOptional, isRequired, mustBeEmpty } from '../../mode-fields';
import { useToastManager } from '../../hooks/use-toast-manager';
import {
  FormProvider,
  useFieldArray,
  useForm,
  useFormContext,
} from 'react-hook-form';
import { notFoundRedirect } from '../404';
import { navigate } from '@reach/router';
import getStats from '../../components/leaderboard/stats';
import { toTitleCase } from '../../components/string-utilities';
import { ErrorMessage } from '@hookform/error-message';
import Cell from '../../components/cell';
import { ModeDescriptionLink } from '../../components/leaderboard-header';
import { CarnivalTheme } from '../challenges';

function needsVideoProof(games, dryRunResult) {
  if (
    dryRunResult == null ||
    !dryRunResult.rank ||
    !dryRunResult.rankingPoints
  ) {
    return false;
  }

  const hasVideoProof = dryRunResult.proof.some(
    p => p.type === ProofType.Video,
  );

  if (
    games.overall.rankedModes.main.includes(dryRunResult.modeId) ||
    games.ti.rankedModes.world.includes(dryRunResult.modeId)
  ) {
    if (dryRunResult.rankingPoints >= 60 && !hasVideoProof) {
      return true;
    }
  } else if (dryRunResult.rank <= 5 && !hasVideoProof) {
    return true;
  }

  return false;
}

function ProofReviewText() {
  return (
    <Trans>
      Please review the{' '}
      <span className="underline">
        <a href="/about/proof" target="_blank">
          Proof Policy
        </a>
      </span>{' '}
      to ensure a smooth score validation process.
    </Trans>
  );
}

const FieldOptionalLabel = ({ field }) => {
  if (isOptional(field)) {
    return (
      <>
        (<Trans>Optional</Trans>)
      </>
    );
  }

  return null;
};

const FormValidationErrorLabel = props => {
  return (
    <ErrorMessage
      {...props}
      render={({ message }) => <span className="form-error">⚠ {message}</span>}
    />
  );
};

const CommentForm = () => {
  const { register, errors } = useFormContext();

  return (
    <div className="form-group">
      <label htmlFor="comment">
        <Trans>Comment</Trans>
        <FormValidationErrorLabel errors={errors} name="comment" />
      </label>
      <textarea
        id="comment"
        name="comment"
        rows="5"
        ref={register({
          maxLength: {
            value: 500,
            message: 'Comment is too long',
          },
        })}
      />
    </div>
  );
};

const ProofForm = () => {
  const { i18n } = useLingui();
  const { register, control, errors } = useFormContext();
  const { fields: proofLinks, append, remove } = useFieldArray({
    control,
    name: 'proof',
  });

  return (
    <div className="form-section__form">
      {proofLinks?.map((proof, index) => (
        <div key={proof.id}>
          <FormValidationErrorLabel name={`proof[${index}].link`} />
          <div className="pack flex-wrap flex-gap-1 flex-wrap-gap-1">
            <div className="form-group">
              <select
                className="select"
                name={`proof[${index}].type`}
                defaultValue={proof.type}
                ref={register({ required: 'This is required.' })}
                required
              >
                <option value="">{i18n._(t`Select type`)}</option>
                <option value={ProofType.Video}>{i18n._(t`Video`)}</option>
                <option value={ProofType.Image}>{i18n._(t`Image`)}</option>
                <option value={ProofType.Other}>{i18n._(t`Other`)}</option>
              </select>
            </div>
            <div className="form-group flex-grow-1">
              <input
                name={`proof[${index}].link`}
                defaultValue={proof.link}
                type="url"
                placeholder="https://example.com"
                ref={register({ required: 'This is required.' })}
                required
              />
            </div>

            <div className="form-group">
              {index > 0 && (
                <button
                  type="button"
                  className="button"
                  onClick={() => remove(index)}
                  disabled={index === 0}
                >
                  <Trans>Remove</Trans>
                </button>
              )}
            </div>
          </div>
        </div>
      ))}
      <div className="form-group span-1">
        <button type="button" className="button" onClick={append}>
          <Trans>Add</Trans>
        </button>
      </div>
    </div>
  );
};

export function UpdateScorePage({ scoreId }) {
  useAuth();

  const toastManager = useToastManager();
  const { user } = useUser();

  const { games } = useGames();

  const formMethods = useForm({
    defaultValues: { proof: [{ type: '', link: '' }] },
    criteriaMode: 'all',
  });
  const { register, handleSubmit, setError, errors, reset } = formMethods;

  const [shouldShowConfirmation, setShowConfirmation] = useState(false);
  const [form, setForm] = useState(null);

  const [scoreData, setScoreData] = useState(null);
  useEffect(() => {
    async function fetchScoreData() {
      const data = await TgmRank.getScore(scoreId);
      setScoreData(data);
    }

    fetchScoreData().catch(() => {
      notFoundRedirect('We could not find the score you were looking for.');
    });
  }, [scoreId]);

  useEffect(() => {
    reset({
      scoreId: Number(scoreData?.scoreId),
      comment: scoreData?.comment?.trim(),
      proof: scoreData?.proof,
      platformId: scoreData?.platform?.platformId,
    });
  }, [scoreData]);

  function getJsonForm(data) {
    const correctedForm = {
      scoreId: Number(data.scoreId),
      proof: data.proof ?? [],
      comment: data.comment?.trim(),
      platformId: data.platformId ? Number(data.platformId) : null,
    };

    setForm(correctedForm);
    setShowConfirmation(true);
  }

  async function onSubmit(form, dryRun) {
    try {
      const updateResult = await TgmRank.updateScore({ ...form, dryRun });

      if (!dryRun) {
        toastManager.addSuccess('Your score has been updated!');
        navigate(`/score/${scoreId}`);
      }

      return updateResult;
    } catch (e) {
      if (e.dataErrors) {
        for (const error of e.dataErrors) {
          setError(error.field, { type: 'manual', message: error.details });
        }
      }
      toastManager.addError('An error occurred while updating your score.', e);
    }

    return null;
  }

  return (
    <Layout>
      <FormProvider {...formMethods}>
        <ConfirmSubmit
          form={form}
          show={shouldShowConfirmation}
          close={() => setShowConfirmation(false)}
          action={() => onSubmit(form)}
          validate={() => onSubmit(form, true)}
        />
        <form className="add-score">
          <Helmet>
            <title>Update score</title>
          </Helmet>
          <header className="add-score__header">
            <h2>
              <Trans>Update score</Trans>
            </h2>
          </header>
          <div className="add-score__content">
            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Score</Trans>
                </h4>
                <p>
                  <Trans>Current details</Trans>
                </p>
              </div>
              <div className="form-section__form">
                {scoreData && games && (
                  <Cell
                    score={scoreData}
                    stats={getStats({
                      mode: games.lookup?.(scoreData.gameId, scoreData.modeId)
                        .mode,
                      showOptional: true,
                    })}
                  />
                )}
              </div>
            </div>
            <input type="hidden" name="scoreId" ref={register} required />
            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Score Details</Trans>
                </h4>
                <p>
                  <Trans>Update some stuff</Trans>
                </p>
              </div>
              <div className="form-section__form">
                <PlatformInput
                  modeId={scoreData?.modeId}
                  includeUnknown={scoreData?.status === ScoreStatus.Legacy}
                />
                <CommentForm />
              </div>
            </div>
            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Proof</Trans>
                </h4>
                <p>
                  <Trans>Show us your (new) moves.</Trans>
                  <br />
                  <ProofReviewText />
                </p>
              </div>
              <ProofForm />
            </div>
          </div>
          <div className="add-score__footer">
            <button
              type="submit"
              className="button"
              onClick={handleSubmit(getJsonForm)}
            >
              <Trans>Submit</Trans>
            </button>
          </div>
        </form>
      </FormProvider>
    </Layout>
  );
}

function correctScoreFormData(user, form, mode) {
  return {
    playerId: isAdmin(user) && form.playerId ? Number(form.playerId) : null,
    modeId: form.modeId ? Number(form.modeId) : null,
    gradeId:
      form.gradeId && !mustBeEmpty(mode?.gradeRequired)
        ? Number(form.gradeId)
        : null,
    level:
      form.level && !mustBeEmpty(mode?.levelRequired)
        ? Number(form.level)
        : null,
    playtime:
      form.playtime && !mustBeEmpty(mode?.timeRequired)
        ? form.playtime?.trim()
        : null,
    score:
      form.score && !mustBeEmpty(mode?.scoreRequired)
        ? Number(form.score)
        : null,
    platformId: form.platformId ? Number(form.platformId) : null,
    proof: form.proof ?? [],
    comment: form.comment?.trim(),
  };
}

function PlayerInput() {
  const { i18n } = useLingui();
  const { setValue, register, control, errors } = useFormContext();
  const { user } = useUser();
  const [players, setPlayers] = useState(null);

  useEffect(() => {
    async function fetchPlayers() {
      const players = await TgmRank.getPlayers();
      setPlayers(players);
    }
    if (user == null) {
      return;
    }

    if (players == null && isAdmin(user)) {
      fetchPlayers().then(() => {
        setValue('playerId', user.userId.toString());
      });
    }
  }, [user]);

  return (
    isAdmin(user) && (
      <div className="form-section">
        <div className="form-section__info">
          <h4>
            <Trans>Player</Trans>
          </h4>
          <p>
            <Trans>Who did this?</Trans>
          </p>
        </div>
        <div className="form-section__form">
          <div className="form-group">
            <label htmlFor="player">Player</label>
            <br />
            <select
              id="player"
              className="select"
              name="playerId"
              ref={register({
                required: true,
              })}
              required={true}
            >
              <option value="">{i18n._(t`Select player`)}</option>
              {players?.map(player => (
                <option key={player.playerId} value={player.playerId}>
                  {player.playerName}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    )
  );
}

function PlatformInput({ modeId, id, name, includeUnknown }) {
  const { register, control, errors } = useFormContext();
  const [modePlatformOptions, setModePlatformOptions] = useState(null);

  useEffect(() => {
    async function fetchPlatforms() {
      if (modeId == null) {
        setModePlatformOptions(null);
        return;
      }

      const platforms = await TgmRank.getModePlatforms(modeId);
      setModePlatformOptions(platforms);
    }

    fetchPlatforms();
  }, [modeId]);

  if (
    modeId == null ||
    modePlatformOptions == null ||
    modePlatformOptions.length === 0
  ) {
    return null;
  }

  return (
    <div className="form-group span-6">
      <label htmlFor={id ?? 'platform'}>
        Platform
        <FormValidationErrorLabel errors={errors} name="platform" />
      </label>
      <select
        id={id ?? 'platform'}
        className="select"
        name={name ?? 'platformId'}
        ref={register({
          required: modePlatformOptions?.length > 0 && !includeUnknown,
        })}
        required={modePlatformOptions?.length > 0 && !includeUnknown}
      >
        <option value="">Select Platform</option>
        {includeUnknown && <option value="">Unknown</option>}
        {modePlatformOptions &&
          modePlatformOptions.map(p => (
            <option key={p.platformId} value={p.platformId}>
              {p.platformName}
            </option>
          ))}
      </select>
    </div>
  );
}

function GradeInput({ mode, id, name }) {
  const { register, control, errors } = useFormContext();
  const [grades, setGrades] = useState(null);

  useEffect(() => {
    async function fetchGrades() {
      if (mode == null) {
        setGrades(null);
        return;
      }

      const grades = await TgmRank.getGrades(mode.modeId);
      const sortedGrades = grades.sort((a, b) => {
        return a.gradeOrder - b.gradeOrder;
      });
      setGrades(sortedGrades);
    }

    fetchGrades();
  }, [mode]);

  if (mode == null || mustBeEmpty(mode?.gradeRequired)) {
    return null;
  }

  return (
    <div className="form-group span-6">
      <label htmlFor={id ?? 'grade'}>
        <GradeTitle modeId={mode.modeId} />{' '}
        <FieldOptionalLabel field={mode?.gradeRequired} />
        <FormValidationErrorLabel errors={errors} name="grade" />
      </label>
      <select
        id={id ?? 'grade'}
        className="select"
        name={name ?? 'gradeId'}
        ref={register({
          required: isRequired(mode?.gradeRequired),
        })}
        required={isRequired(mode?.gradeRequired)}
      >
        <option value="">Select Grade</option>
        {grades &&
          grades.map(g => (
            <option key={g.gradeId} value={g.gradeId}>
              {g.gradeDisplay} {g.line && `(${toTitleCase(g.line)} Line)`}
            </option>
          ))}
      </select>
    </div>
  );
}

function LevelInput({ mode, id, name }) {
  const { register, control, errors } = useFormContext();
  if (mode == null || mustBeEmpty(mode?.levelRequired)) {
    return null;
  }
  return (
    <div className="form-group span-6">
      <label htmlFor={id ?? 'level'}>
        <LevelTitle modeId={mode.modeId} />{' '}
        <FieldOptionalLabel field={mode?.levelRequired} />
        <FormValidationErrorLabel errors={errors} name="level" />
      </label>
      {mode?.modeId === 50 && <LevelCalculator />}
      <input
        id={id ?? 'level'}
        name={name ?? 'level'}
        type="number"
        max="4995"
        placeholder="0"
        ref={register({
          required: isRequired(mode?.levelRequired),
        })}
        required={isRequired(mode?.levelRequired)}
        readOnly={mode?.modeId === 50}
      />
    </div>
  );
}

function TimeInput({ mode, id, name }) {
  const { register, control, errors } = useFormContext();

  if (mode == null || mustBeEmpty(mode?.timeRequired)) {
    return null;
  }
  return (
    <div className="form-group span-6">
      <label htmlFor={id ?? 'playtime'}>
        <TimeTitle /> <FieldOptionalLabel field={mode?.timeRequired} />
        <FormValidationErrorLabel errors={errors} name="playtime" />
      </label>
      {mode?.modeId === 50 && <TimeCalculator />}
      <input
        id={id ?? 'playtime'}
        name={name ?? 'playtime'}
        type="text"
        pattern="[0-5]?[0-9]:[0-5][0-9][:.]\d\d"
        ref={register({
          required: isRequired(mode?.timeRequired),
        })}
        required={isRequired(mode?.timeRequired)}
        placeholder="xx:xx.xx"
        autoComplete="off"
        readOnly={mode?.modeId === 50}
      />
    </div>
  );
}

function ScoreInput({ mode, id, name }) {
  const { register, control, errors } = useFormContext();
  if (mode == null || mustBeEmpty(mode?.scoreRequired)) {
    return null;
  }

  return (
    <div className="form-group span-6">
      <label htmlFor={id ?? 'score'}>
        <ScoreTitle modeId={mode.modeId} />{' '}
        <FieldOptionalLabel field={mode?.scoreRequired} />
        <FormValidationErrorLabel errors={errors} name="score" />
      </label>
      <input
        id={id ?? 'score'}
        name={name ?? 'score'}
        type="number"
        max="999999"
        placeholder="126000"
        ref={register({
          required: isRequired(mode?.scoreRequired),
        })}
        required={isRequired(mode?.scoreRequired)}
      />
    </div>
  );
}

function LevelCalculator() {
  const { setValue: setParentValue } = useFormContext();
  const [levels, setLevels] = useState([0, 0, 0]);
  useEffect(() => {
    if (levels.some(l => l == null || l === 0)) {
      return;
    }
    setParentValue(
      'level',
      levels.reduce((acc, v) => acc + v, 0),
    );
  }, [levels]);

  return (
    <div className="pack flex-gap-1" style={{ marginBottom: '0.5rem' }}>
      <input
        type="number"
        min={6}
        max={999}
        placeholder="First Run Level"
        onChange={event =>
          setLevels(l => [Number(event.target.value), ...l.slice(1)])
        }
        required={true}
      />
      <input
        type="number"
        min={6}
        max={999}
        placeholder="Second Run Level"
        onChange={event =>
          setLevels(l => [l[0], Number(event.target.value), l[2]])
        }
        required={true}
      />
      <input
        type="number"
        min={6}
        max={999}
        placeholder="Third Run Level"
        onChange={event =>
          setLevels(l => [l[0], l[1], Number(event.target.value)])
        }
        required={true}
      />
    </div>
  );
}

function TimeCalculator() {
  const { setValue: setParentValue } = useFormContext();
  const [times, setTimes] = useState([null, null, null]);

  function parseTime(str) {
    const timeRegex = /(?<minutes>[0-5]?[0-9]):(?<seconds>[0-5][0-9])[:.](?<milliseconds>\d\d)/;
    const match = str.match(timeRegex);

    if (match?.groups == null) {
      return null;
    }

    const { minutes, seconds, milliseconds } = match.groups;

    return {
      minutes: Number(minutes),
      seconds: Number(seconds),
      milliseconds: Number(milliseconds),
    };
  }

  useEffect(() => {
    if (times.some(t => t == null)) {
      setParentValue('playtime', null);
      return;
    }

    const sumTime = {
      minutes: 0,
      seconds: 0,
      milliseconds: 0,
    };
    for (const time of times) {
      sumTime.minutes += time.minutes;
      sumTime.seconds += time.seconds;
      sumTime.milliseconds += time.milliseconds;
    }

    sumTime.seconds += Math.trunc(sumTime.milliseconds / 100);
    sumTime.milliseconds %= 100;

    sumTime.minutes += Math.trunc(sumTime.seconds / 60);
    sumTime.seconds %= 60;

    setParentValue(
      'playtime',
      `${sumTime.minutes
        .toString()
        .padStart(2, '0')}:${sumTime.seconds
        .toString()
        .padStart(2, '0')}:${sumTime.milliseconds.toString().padStart(2, '0')}`,
    );
  }, [times]);

  return (
    <div className="pack flex-gap-1" style={{ marginBottom: '0.5rem' }}>
      <input
        type="text"
        placeholder="First Run Time"
        pattern="[0-5]?[0-9]:[0-5][0-9][:.]\d\d"
        onChange={event =>
          setTimes(l => [parseTime(event.target.value), ...l.slice(1)])
        }
        required={true}
      />
      <input
        type="text"
        placeholder="Second Run Time"
        pattern="[0-5]?[0-9]:[0-5][0-9][:.]\d\d"
        onChange={event =>
          setTimes(l => [l[0], parseTime(event.target.value), l[2]])
        }
        required={true}
      />
      <input
        type="text"
        placeholder="Third Run Time"
        pattern="[0-5]?[0-9]:[0-5][0-9][:.]\d\d"
        onChange={event =>
          setTimes(l => [l[0], l[1], parseTime(event.target.value)])
        }
        required={true}
      />
    </div>
  );
}

export default function AddScorePage() {
  useAuth();
  const { user } = useUser();
  const toastManager = useToastManager();
  const formMethods = useForm({
    defaultValues: { proof: [{ type: '', link: '' }] },
    criteriaMode: 'all',
  });
  const {
    register,
    handleSubmit,
    setError,
    errors,
    reset,
    setValue,
  } = formMethods;

  const { i18n } = useLingui();

  let { games } = useGames();

  const [game, setGame] = useState(null);
  const [mode, setMode] = useState(null);
  const [isAlertOpen, setAlert] = useState(false);
  const [shouldShowConfirmation, setShowConfirmation] = useState(false);
  const [isProcessing, setProcessing] = useState(false);
  const [form, setForm] = useState(null);

  function resetAll() {
    reset();
    setGame(null);
    setMode(null);
    setShowConfirmation(false);
    setForm(null);
  }

  function onGameSelect(event) {
    const gameId = event.target.value;
    if (gameId === '') {
      setGame(null);
      setMode(null);
      setValue('modeId', null);
      return;
    }

    const game = games?.find(g => Number(gameId) === g.gameId);
    setGame(game);
    setMode(null);
    setValue('modeId', null);
  }

  async function onModeSelect(event) {
    const modeId = event.target.value;
    if (modeId === '') {
      setMode(null);
      return;
    }

    const mode = game?.modes?.find(m => Number(modeId) === m.modeId);
    setMode(mode);
  }

  function showAlert(event) {
    event.preventDefault();
    setAlert(true);
  }

  function getJsonForm(data) {
    setForm(correctScoreFormData(user, data, mode));
    setShowConfirmation(true);
  }

  async function onSubmit(form, dryRun) {
    try {
      setProcessing(true);
      const submitResult = await TgmRank.submitScore({ ...form, dryRun });

      if (!dryRun) {
        resetAll();
        toastManager.addSuccess('Your score has been submitted!');
      }

      return submitResult;
    } catch (e) {
      if (e.dataErrors) {
        for (const error of e.dataErrors) {
          setError(error.field, { type: 'manual', message: error.details });
        }
      }
      toastManager.addError(
        'An error occurred while submitting your score.',
        e,
      );
    } finally {
      setProcessing(false);
    }

    return null;
  }

  return (
    <Layout>
      {mode?.tags?.includes(ModeTags.Carnival) && (
        <>
          <CarnivalTheme />
        </>
      )}
      <FormProvider {...formMethods}>
        <ConfirmSubmit
          form={form}
          show={shouldShowConfirmation}
          close={() => setShowConfirmation(false)}
          action={() => onSubmit(form)}
          validate={() => onSubmit(form, true)}
        />
        <form className="add-score">
          <Helmet>
            <title>Submit score</title>
          </Helmet>
          <ConfirmReset
            show={isAlertOpen}
            close={() => setAlert(false)}
            action={resetAll}
          />
          <header className="add-score__header">
            <h2>
              <Trans>Add score</Trans>
            </h2>
          </header>
          <div className="add-score__content">
            {isAdmin(user) && <PlayerInput />}
            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Game</Trans>
                </h4>
                <p>
                  <Trans>
                    Choose which game and mode the score was achieved on.
                  </Trans>
                </p>
              </div>
              <div className="form-section__form">
                <div className="form-group">
                  <label htmlFor="game">
                    <Trans>Game</Trans>
                  </label>
                  <select
                    id="game"
                    className="select"
                    name="gameId"
                    onChange={onGameSelect}
                    ref={register({ required: 'This is required.' })}
                  >
                    <option value="">{i18n._(t`Select game`)}</option>
                    {games
                      ?.filter(g => g.gameName !== 'Overall')
                      ?.map(game => (
                        <option key={game.gameName} value={game.gameId}>
                          {game.gameName}
                        </option>
                      ))}
                  </select>
                </div>
                <div className="form-group">
                  <label htmlFor="mode">
                    <Trans>Mode</Trans>
                    {mode && (
                      <>
                        {' '}
                        <ModeDescriptionLink modeId={mode.modeId}>
                          (<Trans>View Rules</Trans>)
                        </ModeDescriptionLink>
                      </>
                    )}
                  </label>
                  <select
                    id="mode"
                    className="select"
                    name="modeId"
                    onChange={onModeSelect}
                    disabled={!game}
                    ref={register({ required: 'This is required.' })}
                  >
                    <option value="">{i18n._(t`Select mode`)}</option>
                    {game?.modes
                      .filter(mode => mode.isActive())
                      .map(mode => (
                        <option key={mode.modeName} value={mode.modeId}>
                          {mode.modeName}
                        </option>
                      ))}
                  </select>
                </div>
                <div className="form-group">
                  <PlatformInput modeId={mode?.modeId} />
                </div>
              </div>
            </div>

            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Score Details</Trans>
                </h4>
                <p>
                  <Trans>Tell us more about your achievement.</Trans>
                </p>
              </div>

              <div className="form-section__form">
                <GradeInput mode={mode} />
                <LevelInput mode={mode} />
                <TimeInput mode={mode} />
                <ScoreInput mode={mode} />
                <CommentForm />
              </div>
            </div>
            <div className="form-section">
              <div className="form-section__info">
                <h4>
                  <Trans>Proof</Trans>
                </h4>
                <p>
                  <Trans>Show us your moves.</Trans>
                  <br />
                  <Trans>
                    Please review the{' '}
                    <Link to="/about/proof">Proof Policy</Link> to ensure a
                    smooth score validation process.
                  </Trans>
                </p>
              </div>
              <ProofForm />
            </div>
          </div>
          <div className="add-score__footer">
            <button type="button" className="button" onClick={showAlert}>
              <Trans>Reset Form</Trans>
            </button>
            <button
              type="submit"
              className="button"
              disabled={isProcessing}
              onClick={handleSubmit(getJsonForm)}
            >
              {isProcessing ? <Trans>...</Trans> : <Trans>Submit</Trans>}
            </button>
          </div>
        </form>
      </FormProvider>
    </Layout>
  );
}

function ConfirmReset({ show, close, action }) {
  const cancelRef = createRef();
  return (
    show && (
      <AlertDialog onDismiss={close} leastDestructiveRef={cancelRef}>
        <AlertDialogLabel>
          <Trans>Reset Form</Trans>
        </AlertDialogLabel>

        <AlertDialogDescription>
          <Trans>Are you sure you want to reset this form?</Trans>
        </AlertDialogDescription>

        <div className="alert-buttons">
          <button
            className="button"
            onClick={() => {
              action();
              close();
            }}
          >
            <Trans>Reset Form</Trans>
          </button>
          <button className="button" ref={cancelRef} onClick={close}>
            <Trans>Cancel</Trans>
          </button>
        </div>
      </AlertDialog>
    )
  );
}

function ConfirmSubmit({ form, show, close, action, validate }) {
  const { games } = useGames();
  const [isProcessing, setProcessing] = useState(false);
  const [dryRun, setDryRun] = useState(null);

  useEffect(() => {
    async function submit() {
      const validateResult = await validate();
      if (validateResult == null) {
        setDryRun(null);
        close();
      } else {
        setDryRun(validateResult);
      }
    }

    if (form != null) {
      setProcessing(true);
      submit();
      setProcessing(false);
    }
  }, [form]);

  if (!dryRun) {
    return null;
  }

  const cancelRef = createRef();
  return (
    show && (
      <AlertDialog
        className="min-w-1-3"
        onDismiss={close}
        leastDestructiveRef={cancelRef}
      >
        <AlertDialogLabel>
          <h1>
            <Trans>Preview</Trans>
          </h1>
        </AlertDialogLabel>

        <AlertDialogDescription>
          {dryRun && (
            <div className="add-score__preview">
              <Cell
                score={dryRun}
                stats={getStats({
                  mode: games.lookup?.(dryRun.gameId, dryRun.modeId).mode,
                  showOptional: true,
                })}
                disableClick={true}
              />
            </div>
          )}
          {dryRun && needsVideoProof(games, dryRun) && (
            <>
              <p>
                <Trans>This score may need video proof.</Trans>
              </p>
              <ProofReviewText />
            </>
          )}
        </AlertDialogDescription>

        <div className="alert-buttons">
          {dryRun && (
            <button
              className="button"
              disabled={isProcessing}
              onClick={() => {
                action();
                close();
              }}
            >
              <Trans>Submit</Trans>
            </button>
          )}
          <button className="button" ref={cancelRef} onClick={close}>
            <Trans>Cancel</Trans>
          </button>
        </div>
      </AlertDialog>
    )
  );
}
