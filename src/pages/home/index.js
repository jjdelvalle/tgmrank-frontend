import { Trans } from '@lingui/macro';
import { useLingui } from '@lingui/react';
import { Menu, MenuButton, MenuItem, MenuList } from '@reach/menu-button';
import React, { useState } from 'react';
import { Helmet } from 'react-helmet-async';
import LongDate from '../../components/date';
import Layout from '../../components/layout';
import { AggregateLeaderboardLabel } from '../../components/leaderboard/label';
import Link from '../../components/link';
import ModeName, { GameLink, ModeLink } from '../../components/mode-name';
import RecentActivity from '../../components/recent-activity';
import { ModeTags, RankedModeKeys, useGames } from '../../hooks/use-games';
import { Info as InfoIcon, X as XIcon } from 'react-feather';

import './styles.css';
import useLocalStorage from '../../hooks/use-storage';

export function GameLeaderboardNavList({ game, modeFilter = () => true }) {
  return (
    <ul>
      {game.modes?.filter(modeFilter).map(mode => (
        <li key={mode.modeName}>
          <ModeLink game={game} mode={mode}>
            {mode?.tags?.includes(ModeTags.Carnival) ? (
              <span className="carnival-theme highlight">
                <ModeName {...mode} />
              </span>
            ) : (
              <ModeName {...mode} />
            )}
            {mode?.tags?.includes(ModeTags.Event) && (
              <span className="badge">Event</span>
            )}
          </ModeLink>
        </li>
      ))}
    </ul>
  );
}

export function LeaderboardNavList({ modeFilter = () => true }) {
  const { i18n } = useLingui();
  const { games } = useGames();

  return (
    <div className="homepage__nav">
      <h2>
        <Trans>Browse Leaderboards</Trans>
      </h2>
      <ul>
        {games?.map(game => (
          <React.Fragment key={game.gameName}>
            <li>
              <GameLink game={game}>{i18n._(game.gameName)}</GameLink>
            </li>
            <GameLeaderboardNavList game={game} modeFilter={modeFilter} />
          </React.Fragment>
        ))}
      </ul>
    </div>
  );
}

const RecentActivityType = {
  LATEST: '0',
  NOTABLE: '1',
};

function HomePageRecentActivity() {
  const { games } = useGames();

  const RecentActivityLabel = {
    [RecentActivityType.LATEST]: <Trans>Latest Scores</Trans>,
    [RecentActivityType.NOTABLE]: <Trans>Notable Scores</Trans>,
  };

  // TODO Should this be initialized from a user setting?
  const [recentActivityType, setRecentActivityType] = useState(
    RecentActivityType.LATEST,
  );

  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <h2>{RecentActivityLabel[recentActivityType]}</h2>
        <Menu>
          <MenuButton>&#10247;</MenuButton>
          <MenuList>
            {Object.entries(RecentActivityLabel)
              .filter(([raType]) => raType !== recentActivityType.toString())
              .map(([raType, label], i) => (
                <MenuItem
                  key={i}
                  onSelect={() => setRecentActivityType(raType)}
                >
                  {label}
                </MenuItem>
              ))}
          </MenuList>
        </Menu>
      </div>
      {recentActivityType === RecentActivityType.NOTABLE && (
        <p>
          <Trans>
            <em>Notable scores</em> are scores that land in the top 10 of any of
            the{' '}
            <AggregateLeaderboardLabel
              game={games.overall}
              rankedModeKey={RankedModeKeys.main}
            />{' '}
            leaderboards.
          </Trans>
        </p>
      )}
      <RecentActivity
        notableScoresOnly={recentActivityType === RecentActivityType.NOTABLE}
        modeIds={
          recentActivityType === RecentActivityType.NOTABLE
            ? games.overall.rankedModes.main
            : null
        }
      />
    </>
  );
}

function News() {
  const [newsStatus, setNewsStatus] = useLocalStorage('news-banners', {
    proofPolicy: true,
  });
  return (
    <div className="homepage__news">
      {/* <h2>News</h2>
      {newsStatus.proofPolicy && (
        <div className="homepage__news-element v-centered flex-gap-1">
          <InfoIcon className="flex-shrink-0" size={20} />
          <span>
            Check out our new <Link to="/about/proof">Proof Policy</Link>. It
            will take effect on{' '}
            <strong>
              <LongDate>2021-11-12T15:31:06Z</LongDate>
            </strong>
          </span>
          <button
            className="ml-auto"
            onClick={() => setNewsStatus(ns => ({ ...ns, proofPolicy: false }))}
          >
            <XIcon size={20} />
          </button>
        </div>
      )} */}
    </div>
  );
}

export default function HomePage() {
  return (
    <Layout>
      <Helmet>
        <title>Home</title>
      </Helmet>
      <div className="homepage">
        <News />
        <LeaderboardNavList modeFilter={m => m.isActive() || m.isRecent()} />
        <div>
          <div>
            {/*<h2>Welcome!</h2>*/}
            {/*<AboutSnippet />*/}
            <div className="homepage__recent-activity">
              <HomePageRecentActivity />
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
