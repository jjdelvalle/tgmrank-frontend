import React, { useState, useEffect } from 'react';
import Layout from '../components/layout';
import { Link } from '@reach/router';
import { Helmet } from 'react-helmet-async';
import { Trans } from '@lingui/macro';
import useLocalStorage from '../hooks/use-storage';
import TgmRank from '../tgmrank-api';
import useSWR from 'swr';
import { useUser } from '../contexts/user';
import { ModeDescription } from '../components/leaderboard-header';

// TODO Remove scores
// TODO Add inline scores
function calculateBestSeries(games) {
  let best = 0;
  let bestIndex = 0;
  for (let i = 0; i < games.length - 4; i++) {
    const test = games.slice(i, i + 5).reduce((a, b) => a + b);
    if (test > best) {
      best = test;
      bestIndex = i;
    }
  }
  return [best, bestIndex];
}

const gamesToShow = 50;
export default function DeathSeriesCalculator() {
  const [
    { games },
    setStorage,
    resetStoredGames,
  ] = useLocalStorage('death-series-5', { games: [] });
  const [input, setInput] = useState('');
  const [bestSeries, setBestSeries] = useState(0);
  const [bestIndex, setBestIndex] = useState(0);
  const [newBest, setNewBest] = useState(null);

  const { user, isLoggedIn } = useUser();
  const { data: playerData } = useSWR(
    user?.username ? TgmRank.getPlayerScores(user?.username) : null,
  );

  const seriesPersonalBest =
    playerData?.find(item => item.gameId === 2 && item?.modeId === 8)?.level ??
    0;

  useEffect(() => {
    const [best, bestIndex] = calculateBestSeries(games);

    // If we have some stored scores, make sure we don't immediately detect a
    // new pb
    if (newBest == null) {
      setNewBest(false);
    } else {
      setNewBest(best > bestSeries);
    }
    setBestSeries(best);
    setBestIndex(bestIndex);
  }, [games]);

  function handleAdd(event) {
    event.preventDefault();

    if (input) {
      const newGames = [...games.slice(-gamesToShow - 1), Number(input)];
      setStorage({ games: newGames });
    }
    setInput('');
  }

  function handleInput(event) {
    const { value } = event.target;
    // Reset message when user types to make message appear again if it is a best
    setNewBest(false);
    if (value === '' || (value.length <= 3 && Number(value))) {
      setInput(value);
    }
  }

  return (
    <Layout>
      <Helmet>
        <title>Death Series of 5 Calculator</title>
      </Helmet>
      <form onSubmit={handleAdd}>
        <h2>
          <Trans>
            <Link to="/TAP/Death (Series of 5)">Death Series of 5</Link>{' '}
            Calculator
          </Trans>
        </h2>
        <ModeDescription modeId={8} />
        <h3>
          <Trans>Past {gamesToShow} games</Trans>
        </h3>{' '}
        <p>
          {games.length > 0 ? (
            games
              .map((game, index) => {
                if (index >= bestIndex && index < bestIndex + 5) {
                  return (
                    <b
                      key={index}
                      style={{
                        color: getComputedStyle(document.body).getPropertyValue(
                          '--highlightColor',
                        ),
                      }}
                    >
                      {game}
                    </b>
                  );
                }
                return <span key={index}>{game}</span>;
              })
              .reduce((prev, curr) => [prev, ', ', curr])
          ) : (
            <Trans>No games added yet.</Trans>
          )}
        </p>
        <p>
          <Trans>
            Your best series score is: <b>{bestSeries}</b>
          </Trans>{' '}
          {newBest && (
            <i
              style={{
                color: getComputedStyle(document.body).getPropertyValue(
                  '--highlightColor',
                ),
              }}
            >
              <Trans>You got a new best score!</Trans>
            </i>
          )}
          <br />
          <Trans>Your current score is:</Trans>{' '}
          <b>
            {games.length >= 5 ? games.slice(-5).reduce((a, b) => a + b) : 0}
          </b>
          <br />
          {isLoggedIn && (
            <>
              <Trans>
                Your personal best is: <b>{seriesPersonalBest}</b>
              </Trans>
            </>
          )}
        </p>
        <label htmlFor="add">Add score</label>
        <div
          style={{
            display: 'flex',
            alignItems: 'flex-end',
            width: 300,
          }}
        >
          <input id="add" type="text" value={input} onChange={handleInput} />
          <button
            className="button"
            style={{
              marginLeft: 15,
            }}
          >
            <Trans>Add</Trans>
          </button>
          <button
            className="button"
            style={{
              marginLeft: 15,
            }}
            onClick={() => {
              resetStoredGames();
            }}
          >
            <Trans>Reset</Trans>
          </button>
        </div>
      </form>
    </Layout>
  );
}
