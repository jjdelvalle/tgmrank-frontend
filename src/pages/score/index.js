import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet-async';
import { Link } from '@reach/router';
import { useForm } from 'react-hook-form';
import useSWR from 'swr';
import { Trans } from '@lingui/macro';
import Layout from '../../components/layout';
import { GameModeLink } from '../../components/mode-name';
import getStats, { StatsRow } from '../../components/leaderboard/stats';
import EmbeddedVideo from '../../components/proof/embedded-video';
import { toTitleCase } from '../../components/string-utilities';
import Tag from '../../components/tag';
import { useToastManager } from '../../hooks/use-toast-manager';
import TgmRank, {
  isReviewedStatus,
  ProofType,
  ScoreStatus,
} from '../../tgmrank-api';

import { notFoundRedirect } from '../404';
import LeaderboardRank from '../../components/leaderboard/rank';
import { ScoreStatusDisplay, ScoreStatusIcon } from '../../components/score';
import { canVerifyScores, useUser } from '../../contexts/user';
import PlayerPageLayout from '../../components/player-page-layout';
import { CarnivalTheme } from '../challenges';

import { ProofThumbnails } from './proof-thumbnails';

import './styles.css';
import { ArrowLeft as ArrowLeftIcon } from 'react-feather';
import { ModeTags, useGames } from '../../hooks/use-games';
import LongDate, { LongDateTime } from '../../components/date';
import { OtherProof } from '../../components/proof/other-proof';
import { ImageProof } from '../../components/proof/image-proof';

function getHostName(url) {
  const apiHostName = new URL(url).hostname;
  return apiHostName.split('.').slice(-2).join('.');
}

export default function ScorePage(props) {
  return (
    <Layout>
      <Score {...props} />
    </Layout>
  );
}

function showEditButton(user, scoreDetails) {
  return (
    user?.userId === scoreDetails?.player.playerId || canVerifyScores(user)
  );
}

function Proof({ proof }) {
  const [displayedProof, setDisplayedProof] = useState(0);
  const handleProofChange = proofIndex => {
    setDisplayedProof(proofIndex);
  };

  const handleNext = () => {
    setDisplayedProof(
      displayedProof + 1 === proof?.length ? 0 : displayedProof + 1,
    );
  };
  const handlePrev = () => {
    setDisplayedProof(
      displayedProof === 0 ? proof?.length - 1 : displayedProof - 1,
    );
  };

  let link;

  if (proof?.length > 0) {
    link = new URL(proof?.[displayedProof].link);
  }

  return (
    <>
      {proof?.length > 0 && (
        <div className="score-details__proof">
          {proof.length > 1 && (
            <button onClick={handlePrev} className="score-details__button">
              <ArrowLeftIcon />
            </button>
          )}
          <div className="score-details__proof-container">
            {proof[displayedProof].type === ProofType.Image ? (
              <ImageProof url={link} />
            ) : proof[displayedProof].type === ProofType.Video ? (
              <EmbeddedVideo link={link} />
            ) : proof[displayedProof].type === ProofType.Other ? (
              <OtherProof url={link} />
            ) : null}
          </div>
          {proof.length > 1 && (
            <button onClick={handleNext} className="score-details__button">
              <ArrowLeftIcon style={{ transform: 'rotate(180deg)' }} />
            </button>
          )}
        </div>
      )}
      <ProofThumbnails
        proof={proof}
        displayedProof={displayedProof}
        onClick={handleProofChange}
      />
    </>
  );
}

function ScoreVerificationStatus({ status }) {
  return (
    <strong>
      <ScoreStatusIcon size={16} status={status} />
      <ScoreStatusDisplay status={status} />
    </strong>
  );
}

function ScoreVerifiedBy({ playerName }) {
  return playerName != null ? (
    <Link to={`/player/${playerName}`}>
      <Tag>{playerName}</Tag>
    </Link>
  ) : (
    <Tag>Admin-User</Tag>
  );
}

export function ScoreDisplay({ scoreId }) {
  const { games } = useGames();
  const { user } = useUser();
  const { data: scoreDetails, error: scoreError } = useSWR(
    TgmRank.getScoreUrl(scoreId).toString(),
  );

  const { game, mode } =
    games?.lookup?.(scoreDetails?.gameId, scoreDetails?.modeId) ?? {};
  const stats = getStats({
    mode,
    showOptional: true,
    exclude: ['rankingPoints'],
  });

  return (
    <>
      <Proof proof={scoreDetails?.proof} />
      {/* {(scoreDetails?.rank || scoreDetails?.rankingPoints != null) && ( */}
      {scoreDetails && (
        <div className="score-details__main">
          <div className="score-details__grid score-details__grid--header">
            <div>
              <div className="score-details__game">
                <GameModeLink game={game} mode={mode} />
              </div>
              <div className="score-details__rank">
                {scoreDetails.rankingPoints != null && (
                  <Trans>
                    <strong>{scoreDetails.rankingPoints}</strong> Points
                  </Trans>
                )}
                {scoreDetails.rankingPoints != null && scoreDetails.rank && (
                  <span aria-hidden="true" role="presentation">
                    {' '}
                    &ndash;{' '}
                  </span>
                )}
                {scoreDetails.rank && (
                  <Trans>
                    Ranked{' '}
                    <strong>
                      <LeaderboardRank rank={scoreDetails.rank} />
                    </strong>
                  </Trans>
                )}
              </div>
            </div>
            <StatsRow stats={stats} item={scoreDetails} />
          </div>
          <div className="score-details__info">
            {scoreDetails.platform?.platformName && (
              <div>
                <div className="underline">
                  <Trans>Platform: {scoreDetails.platform.platformName}</Trans>
                </div>
              </div>
            )}
            <div>
              <div className="underline">
                <Trans>
                  Submitted on <LongDate>{scoreDetails.createdAt}</LongDate>
                </Trans>
              </div>
              <div>
                {scoreDetails.comment?.length > 0 && (
                  <div className="score-details__comment">
                    <p>{scoreDetails.comment}</p>
                  </div>
                )}
              </div>
            </div>
            {isReviewedStatus(scoreDetails.status) && (
              <div>
                <Trans>
                  <ScoreVerificationStatus status={scoreDetails.status} /> by{' '}
                  <ScoreVerifiedBy
                    playerName={
                      scoreDetails.verification?.verifiedBy?.playerName
                    }
                  />{' '}
                  on{' '}
                  {scoreDetails.verification?.verifiedOn != null ? (
                    <LongDate>{scoreDetails.verification.verifiedOn}</LongDate>
                  ) : (
                    <span>???</span>
                  )}
                </Trans>
                {scoreDetails.verification?.comment?.length > 0 && (
                  <div className="score-details__comment">
                    <p>{scoreDetails.verification.comment}</p>
                  </div>
                )}
              </div>
            )}
            {(scoreDetails.status === ScoreStatus.Pending ||
              scoreDetails.status === ScoreStatus.Unverified) && (
              <div className="score-details__comment">
                <Trans>This score has not been reviewed yet.</Trans>
              </div>
            )}
          </div>
          {showEditButton(user, scoreDetails) && (
            <div className="score-details__actions">
              <div>
                {canVerifyScores(user) && <VerifyScoreForm scoreId={scoreId} />}
              </div>
              <div>
                <Link to={`/score/${scoreDetails?.scoreId}/edit`}>
                  <Trans>Edit score</Trans>
                </Link>
              </div>
            </div>
          )}
        </div>
      )}
    </>
  );
}

export function VerifyScoreForm({ scoreId }) {
  const toastManager = useToastManager();

  const { data: scoreDetails, error: scoreError, mutate } = useSWR(
    TgmRank.getScoreUrl(scoreId).toString(),
  );

  const { register, handleSubmit, reset } = useForm({
    defaultValues: {
      status: null,
    },
  });

  useEffect(() => {
    reset({
      status: scoreDetails?.status,
      comment: scoreDetails?.verification?.comment,
    });
  }, [scoreDetails]);

  async function onSubmit(data) {
    try {
      let response = await TgmRank.verifyScore(scoreId, data);
      await mutate(response);
      toastManager.addSuccess(
        `Successfully set this score's status to ${data.status}`,
      );
    } catch (e) {
      toastManager.addError(
        "An error occurred while updating this score's status",
        e,
      );
    }
  }

  if (scoreDetails == null) {
    return null;
  }

  return (
    <form className="verification-form" onSubmit={handleSubmit(onSubmit)}>
      <select className="select" name="status" ref={register} required>
        {Object.entries(ScoreStatus)
          .filter(([, v]) => v !== ScoreStatus.Rejected)
          .map(([, v], index) => (
            <option key={index} value={v}>
              {toTitleCase(v)}
            </option>
          ))}
        <option className="separator" disabled>
          &nbsp;
        </option>
        <option value={ScoreStatus.Rejected}>
          {toTitleCase(ScoreStatus.Rejected)}
        </option>
      </select>
      <textarea
        name="comment"
        ref={register}
        rows={5}
        placeholder="Verification Comment"
        style={{ width: '200px' }}
      />
      <button className="button">Submit</button>
    </form>
  );
}

export function Score({ scoreId }) {
  const { games } = useGames();
  const { data: scoreDetails, error: scoreError } = useSWR(
    TgmRank.getScoreUrl(scoreId).toString(),
  );

  const { game, mode } =
    games?.lookup?.(scoreDetails?.gameId, scoreDetails?.modeId) ?? {};

  // if (error) {
  //   toastManager.addError(error);
  //   return `Error! ${error}`;
  // }

  if (scoreError) {
    notFoundRedirect('We could not find the score you were looking for.');
  }

  return (
    <>
      {mode?.tags?.includes(ModeTags.Carnival) && (
        <>
          <CarnivalTheme />
        </>
      )}
      <PlayerPageLayout playerName={scoreDetails?.player.playerName}>
        <Helmet>
          <title>
            {game &&
              mode &&
              `${game.shortName} - ${mode.modeName} by ${scoreDetails?.player.playerName}`}
          </title>
        </Helmet>
        <ScoreDisplay scoreId={scoreId} />
      </PlayerPageLayout>
    </>
  );
}
