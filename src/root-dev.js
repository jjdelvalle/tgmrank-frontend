import { hot } from 'react-hot-loader/root';
import React from 'react';
import App from './app';
import Cookie from 'js-cookie';
import { i18n } from '@lingui/core';
import { I18nProvider } from '@lingui/react';
import { HelmetProvider } from 'react-helmet-async';

import { en, fr, ja, de, pt, es } from 'make-plural/plurals';

const defaultLocale = 'en';
const acceptedLocales = ['en', 'fr', 'ja', 'de', 'pt', 'es'];

i18n.loadLocaleData({
  en: { plurals: en },
  fr: { plurals: fr },
  ja: { plurals: ja },
  de: { plurals: de },
  pt: { plurals: pt },
  es: { plurals: es },
});

let locale = Cookie.get('locale');
if (locale == null || !acceptedLocales.includes(locale.toLowerCase())) {
  locale = defaultLocale;
}

// TODO Try to load catalog from window
let catalogs;
if (window.__I18N_CATALOGS__) {
  catalogs = window.__I18N_CATALOGS__[locale];
} else {
  catalogs = require(`./locale/${locale}/messages.js`);
}

i18n.load(locale, catalogs.messages);
i18n.activate(locale);

const Root = () => (
  <I18nProvider i18n={i18n}>
    <HelmetProvider>
      <App />
    </HelmetProvider>
  </I18nProvider>
);
export default hot(Root);
